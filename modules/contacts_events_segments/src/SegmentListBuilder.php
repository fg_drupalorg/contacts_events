<?php

namespace Drupal\contacts_events_segments;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the event segment entity type.
 */
class SegmentListBuilder extends EntityListBuilder {

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $routeMatch;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    /** @var static $class */
    $class = parent::createInstance($container, $entity_type);
    $class->routeMatch = $container->get('current_route_match');
    $class->redirectDestination = $container->get('redirect.destination');
    return $class;
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $total = $this->buildQuery()->count()->execute();
    $build['summary']['#markup'] = $this->t('Total segments: @total', ['@total' => $total]);
    $build['table'] = parent::render();
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Title');
    $header['status'] = $this->t('Status');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\contacts_events_segments\Entity\SegmentInterface $entity */
    $row['id'] = $entity->id();
    $row['title'] = $entity->toLink();
    $row['status'] = $entity->isEnabled() ? $this->t('Enabled') : $this->t('Disabled');
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach ($operations as $key => $operation) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->buildQuery();

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * Build the query for getting segments for the event.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query for event segments.
   */
  protected function buildQuery(): QueryInterface {
    $query = $this->getStorage()->getQuery()
      ->accessCheck()
      ->sort($this->entityType->getKey('label'));
    $query->condition('event', $this->routeMatch->getRawParameter('contacts_event'));
    return $query;
  }

}

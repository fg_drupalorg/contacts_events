<?php

namespace Drupal\contacts_events_segments\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\views\Plugin\views\filter\InOperator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Filter handler for Segments.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("contacts_event_segment")
 */
class SegmentFilter extends InOperator {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The argument used to build the value options.
   *
   * This is used to make sure we rebuild the options if the available argument
   * is changed.
   *
   * @var mixed
   */
  protected $eventId;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $self = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $self->entityTypeManager = $container->get('entity_type.manager');
    return $self;
  }

  /**
   * {@inheritdoc}
   */
  public function buildExposeForm(&$form, FormStateInterface $form_state) {
    parent::buildExposeForm($form, $form_state);
    $form['value']['#description'] = $this->t('If this view uses an argument for the event, this list will be filtered to the enabled set of segments for the event when an argument is present.');
  }

  /**
   * {@inheritdoc}
   */
  public function getValueOptions() {
    // Reset the options if our event ID is different.
    $event_id = $this->getEventId();
    if ($event_id != $this->eventId) {
      $this->valueOptions = NULL;
    }

    if (isset($this->valueOptions)) {
      return $this->valueOptions;
    }

    $this->valueOptions['any_segment'] = new TranslatableMarkup('- Any day ticket -');

    // If we have an argument to pull the filtered list from, populate the
    // value options.
    if ($event_id) {
      $segments = $this->entityTypeManager->getStorage('contacts_event_segment')
        ->loadByProperties(['event' => $event_id]);

      foreach ($segments as $segment) {
        /** @var \Drupal\contacts_events_segments\Entity\Segment $segment */
        $this->valueOptions[$segment->id()] = $segment->label();
      }
    }

    return $this->valueOptions;
  }

  /**
   * {@inheritdoc}
   */
  protected function exposedTranslate(&$form, $type) {
    parent::exposedTranslate($form, $type);
    // Rename the 'All' option from '- Any -' to '- All tickets -'.
    if (isset($form['#options']['All'])) {
      $form['#options']['All'] = new TranslatableMarkup('- All Tickets -');
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function opSimple() {
    if (in_array('any_segment', $this->value)) {
      // If user has selected 'Any day ticket' then do a not null on
      // the segment field.
      $this->ensureMyTable();
      $this->query->addWhere($this->options['group'], "$this->tableAlias.$this->realField", NULL, 'IS NOT NULL');
    }
    else {
      parent::opSimple();
    }
  }

  /**
   * Get the event ID we are filtering on.
   *
   * @return int|null
   *   The event ID or NULL if we can't find the argument.
   */
  protected function getEventId() {
    if (isset($this->view->argument)) {
      foreach ($this->view->argument as $argument) {
        if ($argument->table == 'contacts_ticket' && $argument->field == 'event') {
          $arg = $argument->getValue();
          if ($arg && $argument->validateArgument($arg)) {
            return $arg;
          }
          return NULL;
        }
      }
    }
    return NULL;
  }

}

<?php

namespace Drupal\contacts_events_segments\Plugin\EntityReferenceSelection;

use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;

/**
 * Plugin description.
 *
 * @EntityReferenceSelection(
 *   id = "default:contacts_event_segment",
 *   label = @Translation("Segment selection"),
 *   group = "default",
 *   entity_types = {"contacts_event_segment"},
 *   weight = 1
 * )
 */
class SegmentSelection extends DefaultSelection {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($match = NULL, $match_operator = 'CONTAINS') {
    /** @var \Drupal\contacts_events\Entity\Ticket $ticket */
    $ticket = $this->configuration['entity'];
    $query = parent::buildEntityQuery($match, $match_operator);
    // Limit to visible segments that are for the same event.
    $query->condition('status', TRUE);
    $query->condition('event', $ticket->get('event')->target_id);
    return $query;
  }

}

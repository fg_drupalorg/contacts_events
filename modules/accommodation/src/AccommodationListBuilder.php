<?php

namespace Drupal\contacts_events_accommodation;

use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a list controller for the accommodation entity type.
 */
class AccommodationListBuilder extends EntityListBuilder {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * The commerce order item entity storage class.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $orderItemStorage;

  /**
   * The currently active route match object.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new AccommodationListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   * @param \Drupal\Core\Entity\EntityStorageInterface $order_item_storage
   *   The commerce order item entity storage class.
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The currently active route match object.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, DateFormatterInterface $date_formatter, RedirectDestinationInterface $redirect_destination, EntityStorageInterface $order_item_storage, RouteMatchInterface $route_match) {
    parent::__construct($entity_type, $storage);
    $this->dateFormatter = $date_formatter;
    $this->redirectDestination = $redirect_destination;
    $this->orderItemStorage = $order_item_storage;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager')->getStorage($entity_type->id()),
      $container->get('date.formatter'),
      $container->get('redirect.destination'),
      $container->get('entity_type.manager')->getStorage('commerce_order_item'),
      $container->get('current_route_match')
    );
  }

  /**
   * Loads entity IDs using a pager sorted by the entity id.
   *
   * @return array
   *   An array of entity IDs.
   */
  protected function getEntityIds() {
    $event = $this->routeMatch->getParameter('contacts_event');

    $query = $this->getStorage()->getQuery()
      ->accessCheck()
      ->condition('event.target_id', $event->id())
      ->sort($this->entityType->getKey('id'));

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('ID');
    $header['title'] = $this->t('Title');
    $header['delegates'] = $this->t('Delegates');
    $header['available'] = $this->t('Available');
    $header['booked'] = $this->t('Booked');
    $header['pending'] = $this->t('Pending');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\contacts_events_accommodation\AccommodationInterface $entity */
    $row['id'] = $entity->id();
    $row['title'] = $entity->label();
    $row['delegates'] = $entity->getDelegatesText();

    $available = $entity->getAvailable();
    $row['available'] = $available;

    $booked = 0;
    $pending = 0;

    // Load count of booked storage from order items.
    $query = $this->orderItemStorage->getAggregateQuery();
    $query->condition('type', 'ce_accom_camping');
    $query->condition('order_id.entity.event.target_id', $entity->getEventId());
    $query->condition('purchased_entity', $entity->id());
    $query->accessCheck();
    $query->aggregate('quantity', 'SUM');
    $query->groupBy('state');

    if ($result = $query->execute()) {
      $quantity_by_status = ['pending' => 0];
      foreach ($result as $status_row) {
        $quantity_by_status[$status_row['state_value']] = $status_row['quantity_sum'];
      }
      $booked = number_format(array_sum($quantity_by_status) - $quantity_by_status['pending']);
      $pending = number_format($quantity_by_status['pending']);
    }

    if (!empty($row['available'])) {
      $percentage = floor($booked / $available * 1000) / 10;
      $row['booked'] = $this->t('@booked [@percentage%]', [
        '@booked' => $booked,
        '@percentage' => $percentage,
      ]);
    }
    else {
      $row['booked'] = $this->t('@booked', [
        '@booked' => $booked,
      ]);
    }

    $row['pending'] = $pending ?: '';

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = [];
    if ($entity->access('update') && $entity->hasLinkTemplate('canonical')) {
      $operations['edit'] = [
        'title' => $this->t('Edit'),
        'weight' => 10,
        'url' => $this->ensureDestination($entity->toUrl('canonical')),
      ];
    }
    $operations = $operations + parent::getDefaultOperations($entity);
    $destination = $this->redirectDestination->getAsArray();
    foreach ($operations as $key => $operation) {
      $operations[$key]['query'] = $destination;
    }
    return $operations;
  }

}

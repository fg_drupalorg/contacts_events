<?php

namespace Drupal\contacts_events_accommodation;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events\PriceCalculator;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;

/**
 * Service to provide information and validation for accommodation.
 */
class AccommodationHelper {

  /**
   * Validation has passed.
   */
  const VALIDATE_OK = 0;

  /**
   * Validation failed: Too little accommodation.
   */
  const VALIDATE_TOO_LITTLE = 1;

  /**
   * Validation failed: Too much accommodation.
   */
  const VALIDATE_TOO_MUCH = 2;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The price calculator service.
   *
   * @var \Drupal\contacts_events\PriceCalculator
   */
  protected $priceCalculator;

  /**
   * The class to use for the booking accommodation helper.
   *
   * @var string
   */
  protected $accommodationHelperClass = BookingAccommodationHelper::class;

  /**
   * Constructs an accommodation helper object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\contacts_events\PriceCalculator $price_calculator
   *   The price calculator service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, PriceCalculator $price_calculator) {
    $this->entityTypeManager = $entity_type_manager;
    $this->priceCalculator = $price_calculator;
  }

  /**
   * Set the accommodation helper class.
   *
   * @param string $accommodationHelperClass
   *   The accommodation helper class to use. Must be an instance of
   *   BookingAccommodationHelperInterface.
   *
   * @return $this
   */
  public function setAccommodationHelperClass(string $accommodationHelperClass) {
    $this->accommodationHelperClass = $accommodationHelperClass;
    return $this;
  }

  /**
   * Get the booking accommodation helper.
   *
   * @return \Drupal\contacts_events_accommodation\BookingAccommodationHelperInterface
   *   The booking accommodation helper for the specific booking.
   */
  public function getBookingHelper(EntityReferenceFieldItemList $items): BookingAccommodationHelperInterface {
    return new $this->accommodationHelperClass($items, $this->entityTypeManager, $this->priceCalculator);
  }

  /**
   * Get the accommodation of the given type, sorted by weight.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The event to get accommodation for.
   * @param string $bundle
   *   The accommodation bundle.
   * @param bool $public
   *   Whether to only include public accommodation.
   *
   * @return \Drupal\contacts_events_accommodation\AccommodationInterface[]
   *   The accommodation, sorted by weight.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getAccommodation(EventInterface $event, string $bundle, bool $public = TRUE) {
    $storage = $this->entityTypeManager->getStorage('c_events_accommodation');

    $query = $storage->getQuery()
      ->accessCheck()
      ->condition('event', $event->id())
      ->condition('bundle', $bundle)
      ->sort('weight', 'ASC')
      ->sort('title', 'ASC');

    if ($public) {
      $query->condition('public', TRUE);
    }

    return $storage->loadMultiple($query->execute());
  }

  /**
   * Validate that an appropriate amount of accommodation is chosen.
   *
   * @param int $delegates
   *   The number of delegates.
   * @param array $quantities
   *   The quantity of accommodation, keyed by accommodation id.
   *
   * @return int
   *   The validation result. Once of the self::VALIDATE_* constants.
   */
  public function validateAccommodationTotal(int $delegates, array $quantities) {
    $min_delegates = 0;
    $max_delegates = NULL;

    /** @var \Drupal\contacts_events_accommodation\AccommodationInterface[] $accommodations */
    $accommodations = $this->entityTypeManager
      ->getStorage('c_events_accommodation')
      ->loadMultiple(array_keys($quantities));
    foreach ($accommodations as $id => $accommodation) {
      $quantity = $quantities[$id];

      if ($min = $accommodation->getMinDelegates()) {
        $min_delegates += $min * $quantity;
      }
      if ($max = $accommodation->getMaxDelegates()) {
        $max_delegates = ($max_delegates ?? 0) + ($max * $quantity);
      }
    }

    // Return our validation status.
    if ($min_delegates > $delegates) {
      return self::VALIDATE_TOO_MUCH;
    }

    if (isset($max_delegates) && $max_delegates < $delegates) {
      return self::VALIDATE_TOO_LITTLE;
    }

    return self::VALIDATE_OK;
  }

}

<?php

namespace Drupal\contacts_events_accommodation\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides local action definitions for accommodation.
 */
class AccommodationActionDeriver extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs an accommodation local actions deriver.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    if (!$this->derivatives) {
      $storage = $this->entityTypeManager->getStorage('c_events_accommodation_type');
      /** @var \Drupal\contacts_events_accommodation\Entity\AccommodationType $accommodation_type */
      foreach ($storage->loadMultiple() as $accommodation_type) {
        $this->derivatives[$accommodation_type->id()] = [
          'title' => new TranslatableMarkup('Add @bundle', [
            '@bundle' => $accommodation_type->label(),
          ]),
          'route_parameters' => [
            $accommodation_type->getEntityTypeId() => $accommodation_type->id(),
          ],
        ] + $base_plugin_definition;
      }
    }
    return $this->derivatives;
  }

}

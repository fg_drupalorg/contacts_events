Vue.component('village-filter', {
  template: `<select v-model="filters.village" @change="$emit('load-bookings')" style="font-weight: normal">
                <option value="unallocated">Un-allocated</option>
                <option value="any">Any Allocated</option>
                <option v-for="village in villages" :value="village.id">
                  {{village.name}}
                </option>
              </select>`,
  props: ['filters', 'villages'],
});

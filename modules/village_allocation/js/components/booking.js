Vue.component('booking', {
  template: `<div>
              <div draggable="true" @dragstart="drag(booking, $event)" :class="['booking', {'stripe': isAlternate}]">
                <input type="checkbox" v-model="booking.selected" />
                <strong><a href="javascript:void(0)" @click="showBookingDetails = true">{{booking.orderNumber}}</a></strong>

                <span v-if="!isPaidInFull" title="Not paid in full" class="paid-in-full-warning">!!</span>


                {{booking.bookingManager.name}}

                <small class="booking-pitches"><strong>Pitches: {{booking.pitches.toFixed(2)}}</strong></small>

                <div class="special-requirements">
                  <span v-for="requirement in booking.specialRequirements"
                        :title="requirement.name"
                        :class="['requirements', 'requirements-' + requirement.icon]">{{requirement.name}}</span>
                </div>
              </div>
              <modal v-if="showBookingDetails">
                <div slot="header">
                  <strong>Group Booking:</strong> <a target="_blank" :href="getBookingUrl(booking.id)">{{booking.orderNumber}}</a>
                  <span v-if="booking.state !== 'paid_in_full'">(Not paid in full)</span>
                  <br />
                  <strong>Booking Manager:</strong> {{booking.bookingManager.name}}
                  <br />
                  <strong>Pitches:</strong> {{booking.pitches}}
                </div>

                <ul>
                  <li v-for="childBooking in booking.childBookings">
                    <strong><a target="_blank" :href="getBookingUrl(childBooking.id)">{{childBooking.orderNumber}}</a></strong> {{childBooking.bookingManager.name}}
                    <span v-if="childBooking.state !== 'paid_in_full'">(Not paid in full)</span>
                  </li>
                </ul>

                <button slot="footer" @click="showBookingDetails = false">Close</button>
            </modal>
             </div>
`,
  props: ['booking', 'groupId', 'isAlternate'],
  data() {
    return {
      showBookingDetails: false
    }
  },
  watch: {
    // Notify everyone up the chain that the booking was selected/deselected.
    'booking.selected': function (selected) {
      this.$emit(selected ? 'booking-selected' : 'booking-deselected', this.booking);
    }
  },
  computed: {
    isPaidInFull() {
      if (this.booking.state !== 'paid_in_full') {
        return false;
      }

      for(const childBooking of this.booking.childBookings) {
        if (childBooking.state !== 'paid_in_full') {
          return false;
        }
      }

      return true;
    }
  },
  methods: {
    drag() {
      this.booking.selected = true;
    },
    getBookingUrl(id) {
      return `/admin/commerce/orders/${id}/edit`;
    }
  },
});

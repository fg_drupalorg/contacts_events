<?php

namespace Drupal\village_allocation\Plugin\rest\resource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\facets\FacetManager\DefaultFacetManager;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides bookings list.
 *
 * @RestResource(
 *   id = "village_allocation_search_facets",
 *   label = @Translation("Village Allocation - Search Facets"),
 *   uri_paths = {
 *     "canonical" = "/admin/village_allocation/search_facets/{event_id}"
 *   }
 * )
 */
class SearchFacetResource extends ResourceBase {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Fact manager.
   *
   * @var \Drupal\facets\FacetManager\DefaultFacetManager
   */
  protected $facetManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, array $serializer_formats, LoggerInterface $logger, EntityTypeManagerInterface $entity_type_manager, DefaultFacetManager $facet_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->entityTypeManager = $entity_type_manager;
    $this->facetManager = $facet_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest'),
      $container->get('entity_type.manager'),
      $container->get('facets.manager')
    );
  }

  /**
   * Gets search facets as json.
   *
   * @param string $event_id
   *   Current event.
   *
   * @return \Drupal\rest\ResourceResponse
   *   Resource containing serailized search facets.
   */
  public function get($event_id) {
    $response = [];
    // Get the facets defined for the VA camping groups view.
    $facets = $this->facetManager->getFacetsByFacetSourceId('search_api:views_block__camping_groups_indexed__block_1');

    foreach ($facets as $facet) {
      /** @var \Drupal\facets\FacetInterface $facet */
      $facet_build = $this->facetManager->build($facet);
      // Facet manager builds a render array. This isn't useful for us, so
      // extract what we need for serialization to json.
      $facet_json = [
        'id' => $facet->id(),
        'name' => $facet->getName(),
        'items' => [],
      ];

      foreach ($facet_build[0]['#items'] as $item) {
        if ($item['#type'] == 'link') {
          $facet_json['items'][] = [
            'name' => $item['#title']['#value'],
            'count' => $item['#title']['#count'],
            'showCount' => $item['#title']['#show_count'],
            // Extract the filter querystring from the url.
            'filter' => $item['#url']->getOption('query')['f'][0],
          ];
        }
      }

      $response[] = $facet_json;
    }

    return (new ResourceResponse($response))
      ->addCacheableDependency([
        '#cache' => ['max-age' => 0],
      ]);
  }

}

<?php

namespace Drupal\village_allocation;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\contacts_events_villages\Entity\VillageGroup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\search_api\Utility\Utility;

/**
 * Service for reindexing village groups.
 *
 * @package Drupal\village_allocation
 */
class VillageGroupIndexer {

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Indexer's constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Immediately reindexes groups.
   *
   * @param \Drupal\contacts_events_villages\Entity\VillageGroup[] $groups
   *   Groups to reindex.
   */
  public function reindexGroupsImmediately(array $groups) {
    $index = $this->getIndex();
    $ids = [];

    foreach ($groups as $group) {
      foreach ($this->buildItemIds($group, TRUE) as $id) {
        $ids[] = $id;
      }
    }

    $items = $index->loadItemsMultiple($ids);
    $index->indexSpecificItems($items);
  }

  /**
   * Reindexes a group.
   *
   * @param \Drupal\contacts_events_villages\Entity\VillageGroup $group
   *   Group to index.
   */
  protected function reindexGroup(VillageGroup $group) {
    $index = $this->getIndex();
    $index->trackItemsUpdated('entity:c_events_village_group', $this->buildItemIds($group, FALSE));
  }

  /**
   * Reindexes the group attached to a booking.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $booking
   *   The booking that was modified.
   */
  public function reindexGroupForBooking(OrderInterface $booking) {
    if (isset($booking->skipGroupIndexing)) {
      return;
    }

    /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $group */
    /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $original_group */
    $group = $booking->get('village_group')->entity;
    $original_group = isset($booking->original) ? $booking->original->get('village_group')->entity : NULL;

    if ($group || $original_group) {
      if ($group) {
        $group->clearComputedFields();
        $this->reindexGroup($group);
      }
      if ($original_group) {
        if (!$group || $group->id() != $original_group->id()) {
          $original_group->clearComputedFields();
          $this->reindexGroup($original_group);
        }
      }
    }
  }

  /**
   * Build the item IDs for the index.
   *
   * @param \Drupal\contacts_events_villages\Entity\VillageGroup $group
   *   Group to index.
   * @param bool $combined
   *   Whether to use the combined item IDs (with the datasource).
   *
   * @return array
   *   The user's item IDs.
   */
  protected function buildItemIds(VillageGroup $group, bool $combined): array {
    $user_id = $group->id();
    $item_ids = [];
    foreach (array_keys($group->getTranslationLanguages()) as $langcode) {
      $item_id = "$user_id:$langcode";
      $item_ids[] = $combined ? Utility::createCombinedId('entity:c_events_village_group', $item_id) : $item_id;
    }
    return $item_ids;
  }

  /**
   * Gets the index.
   *
   * @return \Drupal\search_api\Entity\Index
   *   The index.
   */
  protected function getIndex() {
    return $this->entityTypeManager
      ->getStorage('search_api_index')
      ->load('camping_groups');
  }

}

<?php

namespace Drupal\village_allocation\Controller;

use Drupal\contacts_events\Entity\Event;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for manual village allocation.
 *
 * @package Drupal\village_allocation\Controller
 */
class VillageAllocationController extends ControllerBase {

  /**
   * VA queries.
   *
   * @var \Drupal\village_allocation\VillageAllocationQueries
   */
  protected $queries;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $self = parent::create($container);
    $self->queries = $container->get('village_allocation.queries');
    $self->entityFieldManager = $container->get('entity_field.manager');
    return $self;
  }

  /**
   * Renders the manual allocation screen.
   *
   * @param \Drupal\contacts_events\Entity\Event $contacts_event
   *   Event being allocated.
   *
   * @return array
   *   Render array.
   */
  public function index(Event $contacts_event) {
    if ($count = $this->queries->countBookingsWithoutGroup($contacts_event->id())) {
      // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
      $this->messenger()->addWarning($this->t('Warning: This event has @count bookings with missing camping group information.', [
        '@count' => $count,
      ]));
    }

    if (!$this->groupBookingsEnabled()) {
      $this->messenger()->addError($this->t('Village Allocation can only be used when the Group Bookings module is installed.'));
      return [];
    }

    return [
      '#markup' => '<div id="app" v-cloak></div>',
      '#attached' => [
        'drupalSettings' => [
          'event_id' => $contacts_event->id(),
          'token_url' => Url::fromRoute('system.csrftoken')->toString(),
        ],
        'library' => [
          'village_allocation/village_allocation',
        ],
      ],
    ];
  }

  /**
   * Checks if group bookings are enabled.
   *
   * @return bool
   *   Whether group bookings are enabled.
   */
  private function groupBookingsEnabled() {
    // Can't just check if the module exists, as different clients have
    // different custom modules for groups. Check for the group field instead
    // as this is consistent between all of them.
    $fields = $this->entityFieldManager->getFieldStorageDefinitions('commerce_order');
    return isset($fields['group_booking']);
  }

}

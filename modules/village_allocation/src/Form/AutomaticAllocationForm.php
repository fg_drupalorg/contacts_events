<?php

namespace Drupal\village_allocation\Form;

use Drupal\contacts_events\Entity\Event;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\Url;
use Drupal\village_allocation\AutomaticAllocation;
use Drupal\village_allocation\VillageAllocationQueries;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form that kicks off automatic allocation batch process.
 *
 * @package Drupal\contacts_events_villages\Form
 */
class AutomaticAllocationForm extends FormBase {

  /**
   * Automatic allocation processor.
   *
   * @var \Drupal\village_allocation\AutomaticAllocation
   */
  protected $allocation;

  /**
   * VA queries.
   *
   * @var \Drupal\village_allocation\VillageAllocationQueries
   */
  protected $queries;

  /**
   * {@inheritdoc}
   */
  public function __construct(AutomaticAllocation $allocation, VillageAllocationQueries $queries) {
    $this->allocation = $allocation;
    $this->queries = $queries;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('village_allocation.automatic'),
      $container->get('village_allocation.queries')
    );
  }

  /**
   * Page title callback.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   Page title.
   */
  public function titleCallback() {
    return $this->t('@label Automatic Village Allocation', [
      '@label' => $this->getEvent()->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'village_allocation_automatic';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Event $contacts_event = NULL) {
    $form['intro'] = [];

    $form['intro'][] = [
      '#markup' => '<h3>The Process</h3>',
    ];

    $form['intro'][] = [
      '#markup' => '<p>Villages are in the order they appear on the event edit page. Churches are sorted by postcode (A-Z). For each church, the bookings are assigned to the first of the following that returns a village:</p>',
    ];

    $form['intro'][] = [
      '#theme' => 'item_list',
      '#list_type' => 'ol',
      '#items' => [
        'The first village which already has bookings from this group and has space for the remaining bookings.',
        'The first village which already has bookings from a group with a similar postcode and has space for the remaining bookings.',
        'The first village which has space for the remaining bookings.',
      ],
    ];

    $form['intro'][] = [
      '#markup' => '<h3>Limitations</h3>',
    ];

    $form['intro'][] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        'Only church based groups are allocated as part of the automatic allocation.',
        'Requirements of the group are not taken into account.',
        'Linked churches are not taken into account.',
        'Village hosts are not taken into account.',
        'Village will only be filled up to their automatic fill percentage.',
        "Each village search starts from the first village in the list, not from any matches from the previous step that didn't have capacity.",
        'A similar post code is where the first 1/2 (up to a space) is the same.',
        'Each church starts from the beginning of the list, not from the most recently assigned village.',
      ],
    ];

    $stats = $this->queries->getBookingStats($contacts_event->id());
    $stats = array_merge($stats, $this->queries->getVillageStats($contacts_event->id()));
    $stats['available'] = $stats['size'] - $stats['assigned'];

    $form['stats_heading'] = ['#markup' => '<h3>Stats</h3>'];

    $form['stats'] = [
      '#type' => 'html_tag',
      '#tag' => 'dl',
      'church_label' => [
        '#type' => 'html_tag',
        '#tag' => 'dt',
        '#value' => $this->t('Churches with unassigned bookings:'),
      ],
      'church_value' => [
        '#type' => 'html_tag',
        '#tag' => 'dd',
        '#value' => number_format($stats['churches'], 0),
      ],
      'bookings_label' => [
        '#type' => 'html_tag',
        '#tag' => 'dt',
        '#value' => $this->t('Total unassigned bookings:'),
      ],
      'bookings_value' => [
        '#type' => 'html_tag',
        '#tag' => 'dd',
        '#value' => number_format($stats['bookings'], 0),
      ],
      'pitches_label' => [
        '#type' => 'html_tag',
        '#tag' => 'dt',
        '#value' => $this->t('Total unassigned pitches:'),
      ],
      'pitches_value' => [
        '#type' => 'html_tag',
        '#tag' => 'dd',
        '#value' => number_format($stats['pitches'], 2),
      ],
      'available_label' => [
        '#type' => 'html_tag',
        '#tag' => 'dt',
        '#value' => $this->t('Available pitches:'),
      ],
      'available_value' => [
        '#type' => 'html_tag',
        '#tag' => 'dd',
        '#value' => number_format($stats['available'], 2),
      ],
      'reserved_label' => [
        '#type' => 'html_tag',
        '#tag' => 'dt',
        '#value' => $this->t('Reserved pitches:'),
      ],
      'reserved_value' => [
        '#type' => 'html_tag',
        '#tag' => 'dd',
        '#value' => number_format($stats['reserved'], 2),
      ],
    ];

    $actions = [
      '#type' => 'actions',
    ];

    $actions['submit'] = [
      '#type' => 'submit',
      '#submit' => ['::submitForm'],
      '#value' => $this->t('Start Automatic Allocation'),
    ];

    $actions['cancel'] = [
      '#type' => 'link',
      '#url' => Url::fromRoute('village_allocation', [
        'contacts_event' => $this->getEvent()->id(),
      ]),
      '#title' => $this->t('Go Back'),
    ];

    $form['actions'] = $actions;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Automatic Village Allocation'),
      'operations' => [
        [[$this->allocation, 'process'], [$this->getEvent()]],
      ],
      'finished' => [$this, 'batchFinished'],
    ];
    batch_set($batch);
  }

  /**
   * Callback for automatic allocation batch completion.
   *
   * @param bool $success
   *   Whether the batch completed successfully.
   * @param array $results
   *   Batch results.
   * @param array $operations
   *   Batch operations.
   */
  public function batchFinished($success, array $results, array $operations) {
    if ($success) {
      $this->messenger()->addMessage($this->t('Finished: @success, @skipped and @error.', [
        '@success' => new PluralTranslatableMarkup($results['assigned'], 'one booking assigned', '@count bookings assigned'),
        '@skipped' => new PluralTranslatableMarkup($results['skipped'], 'one booking skipped', '@count bookings skipped'),
        '@error' => new PluralTranslatableMarkup($results['error'], 'one booking failed', '@count bookings failed'),
      ]), $results['error'] ? 'warning' : 'status');
    }
    else {
      $this->messenger()->addError($this->t('Finished with an error.'));
    }

    foreach ($results['messages'] as $message) {
      $this->messenger()->addWarning($message);
    }
  }

  /**
   * Gets the event being allocated.
   *
   * @return \Drupal\contacts_events\Entity\Event
   *   The event being allocated.
   */
  protected function getEvent() : Event {
    return $this->getRouteMatch()->getParameter('contacts_event');
  }

}

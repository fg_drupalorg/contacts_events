<?php

namespace Drupal\village_allocation_test\Plugin\VillageGroupType;

use Drupal\contacts_events_villages\Entity\VillageGroupInterface;
use Drupal\contacts_events_villages\Plugin\VillageGroupTypeBase;
use Drupal\entity\BundleFieldDefinition;

/**
 * Village group type for villages linked with church taxonomy term.
 *
 * This is a test version of the church group. Various clients have modified
 * versions of this group in their own projects.
 *
 * @VillageGroupType(
 *   id = "church",
 *   label = @Translation("I want to camp with my church"),
 * )
 */
class TestChurchGroup extends VillageGroupTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    $fields['church'] = BundleFieldDefinition::create('entity_reference')
      ->setLabel(t('Church'))
      ->setDescription(t('Start typing the name of your church to find it.'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'search_api:taxonomy_term')
      ->setSetting('handler_settings', ['index' => 'church_terms'])
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 99,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 99,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getExisting(VillageGroupInterface $group) : VillageGroupInterface {
    $storage = $this->entityTypeManager->getStorage('c_events_village_group');
    $query = $storage->getQuery()
      ->condition('event', $group->getEventId())
      ->condition('church', $group->get('church')->target_id)
      ->sort('name', 'ASC');
    $query->accessCheck();

    $result = $storage->loadMultiple($query->execute());
    return !empty($result) ? reset($result) : $group;
  }

}

<?php

/**
 * @file
 * Provides views data and hooks for contacts_events_villages module.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Implements hook_views_data().
 */
function contacts_events_villages_views_data() {
  $data = [];

  $data['commerce_order']['host_report_delegates'] = [
    'group' => new TranslatableMarkup('Booking'),
    'title' => new TranslatableMarkup('Host Report Delegates'),
    'field' => [
      'id' => 'contacts_events_villages_host_report_delegates',
      'real field' => 'order_id',
    ],
  ];

  $data['commerce_order__village_group']['table'] = [
    'group' => new TranslatableMarkup('Order'),
    'join' => [
      'commerce_order' => [
        'left_field' => 'order_id',
        'field' => 'entity_id',
        'extra' => [
          [
            'field' => 'deleted',
            'value' => 0,
            'numberic' => TRUE,
          ],
        ],
      ],
    ],
    'entity type' => 'commerce_order',
  ];

  $data['commerce_order__village_group']['village_group'] = [
    'group' => new TranslatableMarkup('Order'),
    'title' => new TranslatableMarkup('Camping Location'),
    'help' => new TranslatableMarkup('The group that a booking is camping with.'),
    'field' => [
      'table' => 'commerce_order__village_group',
      'id' => 'field',
      'field_name' => 'village_group',
      'entity_type' => 'commerce_order',
      'real field' => 'village_group_target_id',
      'additional fields' => [
        'delta',
        'langcode',
        'bundle',
        'village_group_target_id',
      ],
      'element type' => 'div',
      'is revision' => FALSE,
      'click sortable' => TRUE,
    ],
    'relationship' => [
      'title' => new TranslatableMarkup('Camping Location referenced from Booking'),
      'label' => new TranslatableMarkup('Camping Location'),
      'id' => 'standard',
      'base' => 'c_events_village_group',
      'base field' => 'id',
      'relationship field' => 'village_group_target_id',
    ],
  ];

  $data['commerce_order__village_group']['village_group_target_id'] = [
    'group' => new TranslatableMarkup('Order'),
    'title' => new TranslatableMarkup('Camping Location'),
    'argument' => [
      'id' => 'numeric',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'village_group_target_id',
      ],
      'field_name' => 'village_group',
      'entity_type' => 'commerce_order',
      'empty field name' => new TranslatableMarkup('- No value -'),
    ],
    'filter' => [
      'id' => 'contacts_events_order_camping',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'village_group_target_id',
      ],
      'field_name' => 'village_group',
      'entity_type' => 'commerce_order',
      'allow empty' => TRUE,
    ],
    'sort' => [
      'id' => 'standard',
      'additional fields' => [
        'bundle',
        'deleted',
        'entity_id',
        'revision_id',
        'langcode',
        'delta',
        'village_group_target_id',
      ],
      'field_name' => 'village_group',
      'entity_type' => 'commerce_order',
    ],
    'entity field' => 'village_group',
  ];

  // Add a pseudo reverse entity relationship for Orders from Village Groups.
  $data['c_events_village_group']['commerce_order'] = [
    'title' => new TranslatableMarkup('Booking from Camping Location'),
    'relationship' => [
      'label' => new TranslatableMarkup('Booking'),
      'id' => 'entity_reverse',
      'base' => 'commerce_order',
      'base field' => 'order_id',
      'field_name' => 'village_group',
      'field table' => 'commerce_order__village_group',
      'field field' => 'village_group_target_id',
    ],
  ];

  return $data;
}

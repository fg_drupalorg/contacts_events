<?php

namespace Drupal\Tests\contacts_events_villages\Functional;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_store\Entity\Store;
use Drupal\contacts_events\Entity\Event;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events\Plugin\Commerce\CheckoutFlow\BookingFlow;
use Drupal\contacts_events_villages\Entity\VillageGroup;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Test the tickets step of the booking process.
 *
 * @group contacts_events
 */
class VillageBookingWidgetTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'toolbar',
    'contacts_events',
    'contacts_events_accommodation',
    'contacts_events_villages',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   *
   * Https://www.drupal.org/project/drupal/issues/3044920 is resolved.
   */
  protected $strictConfigSchema = FALSE;

  /**
   * The event.
   *
   * @var \Drupal\contacts_events\Entity\EventInterface
   */
  protected $event;

  /**
   * A user with permission to the checkout.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $user;

  /**
   * The booking.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface
   */
  protected $booking;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Set up the event.
    $this->event = Event::create(['type' => 'default']);
    $start = DrupalDateTime::createFromTimestamp(strtotime('+2 months'))
      ->setTime(10, 0, 0);
    $end = (clone $start)
      ->add(new \DateInterval('P1D'))
      ->setTime(17, 0, 0);
    $this->event
      ->set('title', 'Test event')
      ->set('code', 'TEST')
      ->set('date', [
        'value' => $start->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
        'end_value' => $end->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      ])
      ->set('booking_status', EventInterface::STATUS_OPEN)
      ->set('booking_windows', [
        [
          'id' => 'standard',
          'label' => 'Standard',
        ],
      ])
      ->set('ticket_classes', ['child', 'standard'])
      ->set('accommodation_types', ['camping'])
      ->set('village_group_types', ['other'])
      ->set('ticket_price', [
        [
          'number' => 4.99,
          'currency_code' => 'USD',
          'booking_window' => 'standard',
          'class' => 'child',
        ],
        [
          'number' => 9.99,
          'currency_code' => 'USD',
          'booking_window' => 'standard',
          'class' => 'standard',
        ],
      ])
      ->save();

    $this->user = $this->drupalCreateUser([
      'can book for contacts_events',
    ]);
    $this->drupalLogin($this->user);

    $name = $this->randomMachineName(8);
    $mail = $this->user->getEmail();

    $currency_importer = \Drupal::service('commerce_price.currency_importer');
    $currency_importer->import('USD');
    $store = Store::create([
      'type' => 'online',
      'uid' => 1,
      'name' => $name,
      'mail' => $mail,
      'address' => [
        'country_code' => 'US',
        'address_line1' => $this->randomString(),
        'locality' => $this->randomString(5),
        'administrative_area' => 'WI',
        'postal_code' => '53597',
      ],
      'default_currency' => 'USD',
      'billing_countries' => [
        'US',
      ],
    ]);
    $store->save();

    // Create the booking.
    $this->booking = Order::create([
      'type' => 'contacts_booking',
      'store_id' => $store->id(),
      'event' => $this->event,
      'uid' => $this->user->id(),
      'checkout_step' => 'accommodation',
    ]);
    $this->booking->save();
  }

  /**
   * Tests that creating a village group becomes visible for accommodation.
   */
  public function testAddVillageGroupToEmpty() {
    // Create a village group, without event.
    $village = VillageGroup::create([
      'type' => 'other',
      'name' => 'Social village',
    ]);
    $village->save();

    // Load up the accommodation booking page.
    $accommodation_url = Url::fromRoute(BookingFlow::ROUTE_NAME, [
      'commerce_order' => $this->booking->id(),
      'step' => 'accommodation',
    ]);
    $this->drupalGet($accommodation_url);

    // Check the page loaded correctly.
    $assert_session = $this->assertSession();
    $assert_session->statusCodeEquals(200);

    $assert_session->elementExists('css', 'form.commerce-checkout-flow-booking-flow');
    $assert_session->elementExists('css', 'div.checkout-pane-booking-accom-camping');
    $assert_session->pageTextNotContains($village->label());

    // Set event on village group entity.
    $village->set('event', $this->event->id())->save();

    // Reload page.
    $this->drupalGet($accommodation_url);

    // Check that village is now available.
    $this->assertSession()->pageTextContainsOnce($village->label());

    // Remove the other village group type from event settings.
    $this->event->set('village_group_types', [])->save();

    // Reload page.
    $this->drupalGet($accommodation_url);

    // Check that village is not available any more.
    $this->assertSession()->pageTextNotContains($village->label());
  }

  /**
   * Tests that creating a organisation village group deduping.
   */
  public function testOrganisationVillageForm() {
    $this->event->set('village_group_types', ['organisation'])->save();

    // Create some organisations to camp with.
    $org_1 = $this->createUser();
    $org_1->addRole('crm_org');
    $org_1->save();

    $org_2 = $this->createUser();
    $org_2->addRole('crm_org');
    $org_2->save();

    // Load up the accommodation booking page.
    $accommodation_url = Url::fromRoute(BookingFlow::ROUTE_NAME, [
      'commerce_order' => $this->booking->id(),
      'step' => 'accommodation',
    ]);
    $this->drupalGet($accommodation_url);

    // Check the page loaded correctly.
    $assert_session = $this->assertSession();
    $assert_session->statusCodeEquals(200);

    $assert_session->elementExists('css', 'form.commerce-checkout-flow-booking-flow');
    $assert_session->elementExists('css', 'div.checkout-pane-booking-accom-camping');

    // Track the group id changes.
    $order_storage = \Drupal::entityTypeManager()->getStorage('commerce_order');
    $village_groups = [];

    // Create a village group for the first organisation.
    $this->submitForm([
      'booking_accom_camping[camping][village_group][0][target_id]' => 'organisation',
      'booking_accom_camping[camping][village_group][0][subform][organisation][organisation]' => "{$org_1->label()} ({$org_1->id()})",
    ], 'Continue to your details');
    // It is necessary to reload the booking as it has changed.
    $village_groups[] = $order_storage->loadUnchanged($this->booking->id())->get('village_group')->target_id;

    $this->drupalGet($accommodation_url);
    $this->assertSession()->fieldValueEquals('booking_accom_camping[camping][village_group][0][subform][organisation][organisation]', "{$org_1->label()} ({$org_1->id()})");

    // Create a new village group for the second organisation.
    $this->submitForm([
      'booking_accom_camping[camping][village_group][0][target_id]' => 'organisation',
      'booking_accom_camping[camping][village_group][0][subform][organisation][organisation]' => "{$org_2->label()} ({$org_2->id()})",
    ], 'Continue to your details');
    $village_groups[] = $order_storage->loadUnchanged($this->booking->id())->get('village_group')->target_id;

    $this->drupalGet($accommodation_url);
    $this->assertSession()->fieldValueEquals('booking_accom_camping[camping][village_group][0][subform][organisation][organisation]', "{$org_2->label()} ({$org_2->id()})");

    // Re-use the existing village group for the first organisation..
    $this->submitForm([
      'booking_accom_camping[camping][village_group][0][target_id]' => 'organisation',
      'booking_accom_camping[camping][village_group][0][subform][organisation][organisation]' => "{$org_1->label()} ({$org_1->id()})",
    ], 'Continue to your details');
    $village_groups[] = $order_storage->loadUnchanged($this->booking->id())->get('village_group')->target_id;

    $this->drupalGet($accommodation_url);
    $this->assertSession()->fieldValueEquals('booking_accom_camping[camping][village_group][0][subform][organisation][organisation]', "{$org_1->label()} ({$org_1->id()})");

    self::assertNotSame($village_groups[0], $village_groups[1], 'Different village groups created for different names.');
    self::assertSame($village_groups[0], $village_groups[2], 'No village group duplicate created for same organisation.');
  }

}

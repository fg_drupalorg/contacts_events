<?php

namespace Drupal\contacts_events_villages;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Camping village group entities.
 *
 * @ingroup contacts_events_villages
 */
class VillageListBuilder extends EntityListBuilder {

  /**
   * Current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    $self = parent::createInstance($container, $entity_type);
    $self->routeMatch = $container->get('current_route_match');
    return $self;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $query = $this->getStorage()->getQuery()
      ->sort('name');
    $query->accessCheck();

    // Filter by current event.
    if ($event = $this->routeMatch->getParameter('contacts_event')) {
      $query->condition('event', $event->id());
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['name'] = $this->t('Name');
    $header['fill_value'] = $this->t('Max fill value');
    $header['pitches'] = $this->t('Pitches');
    $header['special_requirements'] = $this->t('Special Requirements');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\contacts_events_villages\Entity\Village $entity */
    $row['name']['data'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => ['style' => "color:{$entity->get('colour')->value}"],
      '#value' => $entity->label(),
    ];

    $row['fill_value'] = $entity->get('fill_value')->value;
    $row['pitches'] = $entity->get('pitches')->value;

    $requirements = [];

    foreach ($entity->get('special_requirements') as $item) {
      /** @var \Drupal\taxonomy\Entity\Term $requirement */
      $requirement = $item->entity;
      $requirements[] = $requirement->label();
    }

    $row['special_requirements'] = implode(', ', $requirements);
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    /** @var \Drupal\contacts_events_villages\Entity\Village $entity */
    $operations = parent::getDefaultOperations($entity);

    $url = Url::fromRoute('contacts_events_villages.host_info', [
      'contacts_event' => $entity->getEventId(),
      'village' => $entity->id(),
    ]);
    $operations['host-info'] = [
      'title' => $this->t('Host info'),
      'weight' => 50,
      'url' => $url,
    ];

    return $operations;
  }

}

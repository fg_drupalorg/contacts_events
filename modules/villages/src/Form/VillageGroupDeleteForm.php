<?php

namespace Drupal\contacts_events_villages\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a form for deleting Camping village group entities.
 *
 * @ingroup contacts_events_villages
 */
class VillageGroupDeleteForm extends ContentEntityDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $entity */
    $entity = parent::validateForm($form, $form_state);

    // Check for bookings that already use a village group and prevent deleting
    // if any are found.
    $query = $this->entityTypeManager->getStorage('commerce_order')->getQuery()
      ->condition('type', 'contacts_booking')
      ->condition('village_group', $entity->id());
    $query->accessCheck();

    if (!empty($query->execute())) {
      $error_message = $this->t('You cannot delete a village group that has bookings.');
      $form_state->setErrorByName(NULL, $error_message);
    }

    return $entity;
  }

}

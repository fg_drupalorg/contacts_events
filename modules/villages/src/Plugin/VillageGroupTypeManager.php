<?php

namespace Drupal\contacts_events_villages\Plugin;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides the Village group type plugin manager.
 */
class VillageGroupTypeManager extends DefaultPluginManager {

  /**
   * Constructs a new VillageGroupTypeManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/VillageGroupType', $namespaces, $module_handler, 'Drupal\contacts_events_villages\Plugin\VillageGroupTypeInterface', 'Drupal\contacts_events_villages\Annotation\VillageGroupType');

    $this->alterInfo('contacts_events_villages_c_events_village_group_type_info');
    $this->setCacheBackend($cache_backend, 'contacts_events_villages_c_events_village_group_type_plugins');
  }

}

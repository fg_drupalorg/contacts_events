<?php

namespace Drupal\contacts_events_villages\Plugin\VillageGroupType;

use Drupal\contacts_events_villages\Entity\VillageGroupInterface;
use Drupal\contacts_events_villages\Plugin\VillageGroupTypeBase;

/**
 * Village group type for other villages.
 *
 * @VillageGroupType(
 *   id = "other",
 *   label = @Translation("Other"),
 * )
 */
class OtherGroup extends VillageGroupTypeBase {

  /**
   * {@inheritdoc}
   */
  public function isExpanded() : bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getExisting(VillageGroupInterface $group) : VillageGroupInterface {
    $storage = $this->entityTypeManager->getStorage('c_events_village_group');
    $query = $storage->getQuery()
      ->condition('event', $group->getEventId())
      ->condition('name', $group->getName())
      ->sort('name', 'ASC');
    $query->accessCheck();

    $result = $storage->loadMultiple($query->execute());

    return !empty($result) ? reset($result) : $group;
  }

}

<?php

namespace Drupal\contacts_events_villages\Plugin;

use Drupal\Component\Plugin\PluginBase;
use Drupal\contacts_events_villages\Entity\VillageGroupInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for Village group type plugins.
 */
abstract class VillageGroupTypeBase extends PluginBase implements VillageGroupTypeInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Constructs a VillageGroupTypeBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = [];

    // Add description field for expanded group types.
    if ($this->isExpanded()) {
      $fields['description'] = BundleFieldDefinition::create('string')
        ->setTranslatable(TRUE)
        ->setLabel(new TranslatableMarkup('Description'))
        ->setDescription(new TranslatableMarkup('A description of the village group.'))
        ->setDisplayOptions('form', [
          'type' => 'string_textfield',
          'weight' => -1,
        ])
        ->setDisplayConfigurable('form', TRUE)
        ->setDisplayOptions('view', [
          'type' => 'string',
          'label' => 'above',
          'weight' => -1,
        ])
        ->setDisplayConfigurable('view', TRUE);
    }

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function isExpanded() : bool {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function form(EntityReferenceFieldItemListInterface $items) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function buildFormEntity(array $values, VillageGroupInterface $group) : VillageGroupInterface {
    // Set any fields on the group that have been provided.
    if (!empty($values)) {
      foreach ($values as $field_name => $field_value) {
        if ($group->hasField($field_name)) {
          $group->set($field_name, $field_value);
        }
      }
    }
    return $group;
  }

  /**
   * {@inheritdoc}
   */
  abstract public function getExisting(VillageGroupInterface $group) : VillageGroupInterface;

  /**
   * {@inheritdoc}
   */
  public function validate(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // Default implementation is no-op.
  }

  /**
   * {@inheritdoc}
   */
  public function process(array &$subform, FormStateInterface $form_state, array &$complete_form, $path_to_containing_widget) {
    // Default implementation is no-op.
  }

}

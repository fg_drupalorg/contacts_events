<?php

namespace Drupal\contacts_events_villages\Controller;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events_villages\Entity\Village;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Link;

/**
 * Village host info page.
 *
 * @package Drupal\contacts_events_villages\Controller
 */
class VillageHostInfoController extends ControllerBase {

  /**
   * Checks whether the current user has access to view the host info.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The event.
   * @param \Drupal\contacts_events_villages\Entity\Village|null $village
   *   The village, if we want to check VA assignment.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Result of the access check.
   */
  public function access(EventInterface $contacts_event, Village $village = NULL) {
    // Ensure the event is configured for village hosts.
    if (!$contacts_event->hasField('village_host_teams') || $contacts_event->get('village_host_teams')->isEmpty()) {
      return AccessResult::forbidden()->setCacheMaxAge(0);
    }

    // Allow access if you can manage villages.
    $result = AccessResult::allowedIfHasPermission($this->currentUser(), 'manage c_events_village entities');
    if ($result->isAllowed()) {
      return $result;
    }

    // Village Hosts aren't allowed to view the report if allocations aren't
    // public yet.
    if (!$contacts_event->hasField('make_village_allocation_public') || !$contacts_event->get('make_village_allocation_public')->value) {
      return AccessResult::forbidden()->setCacheMaxAge(0);
    }

    // Must have an approved application for the Village Host team
    // and be allocated to this specific village.
    $query = $this->getApplicationsQuery($contacts_event, $village);
    $result = $result->orIf(AccessResult::allowedIf($query->count()->execute()));

    return $result;
  }

  /**
   * Village host info page.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The event.
   * @param \Drupal\contacts_events_villages\Entity\Village $village
   *   The village.
   *
   * @return array
   *   Render array.
   */
  public function hostInfo(EventInterface $contacts_event, Village $village) {
    $build = [
      // Apply a theme so it can be easily modified.
      '#theme' => 'contacts_events_villages_host_info',
      // Prevent Drupal from caching the page.
      '#cache' => ['max-age' => 0],
    ];

    $build['event_title'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $contacts_event->label(),
    ];
    $build['event_host_info'] = $contacts_event->get('village_host_info')->view();
    $build['event_files'] = $contacts_event->get('village_host_files')->view();

    /** @var \Drupal\contacts_events_villages\Entity\Village $village */
    $build['village_host_info'] = $village->get('village_host_info')->view();
    $build['village_files'] = $village->get('village_host_files')->view();

    if (isset($build['village_host_info']['#title'])) {
      $build['village_host_info']['#title'] .= ' - ' . $village->get('name')->value;
    }
    if (isset($build['village_files']['#title'])) {
      $build['village_files']['#title'] .= ' - ' . $village->get('name')->value;
    }

    $link = Link::createFromRoute(
      'View Host Report',
      'view.contacts_events_village_hosts_report.page_1',
      [
        'contacts_event' => $contacts_event->id(),
        'village' => $village->id(),
      ]
    );
    $build['village_host_report'] = $link->toRenderable();

    return $build;
  }

  /**
   * Gets the query for retrieving team application based on village host teams.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $contacts_event
   *   The event.
   * @param \Drupal\contacts_events_villages\Entity\Village|null $village
   *   The village, if we want to check VA assignment.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query.
   */
  private function getApplicationsQuery(EventInterface $contacts_event, Village $village = NULL) {
    $team_ids = [];

    foreach ($contacts_event->get('village_host_teams') as $item) {
      $team_ids[] = $item->target_id;
    }

    // Must have an approved application for this team.
    $query = $this->entityTypeManager()->getStorage('c_events_team_app')->getQuery();
    $query->accessCheck();
    $query->condition('team', $team_ids, 'IN');
    $query->condition('ticket.entity.contact', $this->currentUser()->id());
    $query->condition('state', 'approved');
    $query->condition('event', $contacts_event->id());

    if ($village) {
      $query->condition('ticket.entity.order_item.entity.order_id.entity.village', $village->id());
    }

    return $query;
  }

}

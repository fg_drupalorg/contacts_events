<?php

namespace Drupal\contacts_events_villages;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\Routing\Route;

/**
 * Provides routes for Camping village group entities.
 */
class VillageGroupHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  public function getRoutes(EntityTypeInterface $entity_type) {
    $collection = parent::getRoutes($entity_type);
    $entity_type_id = $entity_type->id();

    $village_admin_routes = [
      "entity.{$entity_type_id}.canonical",
      "entity.{$entity_type_id}.add_form",
      "entity.{$entity_type_id}.delete_form",
      "entity.{$entity_type_id}.collection",
    ];

    foreach ($collection as $route_id => $route) {
      // Make sure the event parameter is correctly typed for all routes.
      $route->setOption('parameters', [
        'contacts_event' => ['type' => 'entity:contacts_event'],
      ]);

      // Check that event has village groups enabled.
      $route->setRequirement('_contacts_events_village_group', 'TRUE');

      // Make sure admin route is set so we get the admin theme for
      // add/edit/delete/list etc.
      if (in_array($route_id, $village_admin_routes)) {
        $route->setOption('_admin_route', TRUE);
      }
    }

    return $collection;
  }

  /**
   * {@inheritdoc}
   */
  protected function getAddFormRoute(EntityTypeInterface $entity_type) {
    if ($route = parent::getAddFormRoute($entity_type)) {
      $defaults = $route->getDefaults();
      unset($defaults['_title_callback']);
      $defaults['_title'] = (string) new TranslatableMarkup('Add camping village group');
      $route->setDefaults($defaults);
      return $route;
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type) {
    if ($entity_type->hasLinkTemplate('canonical')) {
      $entity_type_id = $entity_type->id();
      $route = new Route($entity_type->getLinkTemplate('canonical'));

      // Use the edit form handler, if available, otherwise default.
      $operation = 'default';
      if ($entity_type->getFormClass('edit')) {
        $operation = 'edit';
      }
      $route
        ->setDefaults([
          '_entity_form' => "{$entity_type_id}.{$operation}",
          '_title_callback' => '\Drupal\Core\Entity\Controller\EntityController::editTitle',
        ])
        ->setRequirement('_entity_access', "{$entity_type_id}.update")
        ->setOption('parameters', [
          $entity_type_id => ['type' => 'entity:' . $entity_type_id],
        ]);

      // Entity types with serial IDs can specify this in their route
      // requirements, improving the matching process.
      if ($this->getEntityTypeIdKeyType($entity_type) === 'integer') {
        $route->setRequirement($entity_type_id, '\d+');
      }
      return $route;
    }
  }

}

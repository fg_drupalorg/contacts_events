<?php

namespace Drupal\contacts_events_villages\Entity;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Calculates whether a group has pitch requirements or not.
 *
 * @package Drupal\contacts_events_villages\Entity
 */
class HasPitchesItemList extends FieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    /** @var \Drupal\contacts_events_villages\Entity\VillageGroup $entity */
    $entity = $this->getEntity();

    $query = \Drupal::entityQuery('commerce_order_item');
    $query->accessCheck(FALSE);
    $query->condition('type', 'ce_accom_camping');
    $query->condition('order_id.entity.village_group.target_id', $entity->id());
    $query->condition('order_id.entity.state', 'draft', '<>');
    $query->condition('purchased_entity.entity:c_events_accommodation.pitch_size', 0, '>');

    $query->count();
    $count = $query->execute();

    $this->list[0] = $this->createItem(0, $count > 0);
  }

  /**
   * Reset the computed value.
   */
  public function resetValue() {
    $this->valueComputed = FALSE;
  }

}

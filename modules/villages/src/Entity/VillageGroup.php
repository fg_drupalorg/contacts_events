<?php

namespace Drupal\contacts_events_villages\Entity;

use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the Camping village group entity.
 *
 * @ingroup contacts_events_villages
 *
 * @ContentEntityType(
 *   id = "c_events_village_group",
 *   label = @Translation("Camping village group"),
 *   label_collection = @Translation("Camping village groups"),
 *   label_singular = @Translation("camping village group"),
 *   labeble_l_plural = @Translation("camping village groups"),
 *   bundle_label = @Translation("Camping village group type"),
 *   bundle_plugin_type = "c_events_village_group_type",
 *   handlers = {
 *     "list_builder" = "Drupal\contacts_events_villages\VillageGroupListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\contacts_events_villages\Form\VillageGroupForm",
 *       "add" = "Drupal\contacts_events_villages\Form\VillageGroupForm",
 *       "edit" = "Drupal\contacts_events_villages\Form\VillageGroupForm",
 *       "delete" = "Drupal\contacts_events_villages\Form\VillageGroupDeleteForm",
 *     },
 *     "access" = "Drupal\contacts_events_villages\VillageGroupAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_events_villages\VillageGroupHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "c_events_village_group",
 *   admin_permission = "manage c_events_village_group entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/event/{contacts_event}/accommodation/village-groups/{c_events_village_group}",
 *     "add-form" = "/event/{contacts_event}/accommodation/village-groups/add/{type}",
 *     "delete-form" = "/event/{contacts_event}/accommodation/village-groups/{c_events_village_group}/delete",
 *     "collection" = "/event/{contacts_event}/accommodation/village-groups",
 *   }
 * )
 */
class VillageGroup extends ContentEntityBase implements VillageGroupInterface {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    if ($this->hasField('description')) {
      return $this->get('description')->value;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    if ($this->hasField('description')) {
      $this->set('description', $description);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setEvent($event) {
    $this->set('event', $event);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEvent() : EventInterface {
    return $this->get('event')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getEventId() : int {
    return $this->get('event')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $parameters = parent::urlRouteParameters($rel);
    $parameters['contacts_event'] = $this->getEventId();
    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Camping village group entity.'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -2,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['event'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Event'))
      ->setDescription(t('The event this village is for.'))
      ->setRequired(TRUE)
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'contacts_event')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('form', ['region' => 'hidden'])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['allocation_sorting_code'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Sorting code'))
      ->setDescription(t('The value to sort on for automatic allocation. Typically a postcode'));

    // Indexed with solr for village allocation.
    $fields['has_requirements'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Has Requirements'))
      ->setDescription(t('Whether this group has specific camping requirements'))
      ->setComputed(TRUE)
      ->setClass(HasRequirementsItemList::class);

    // Indexed with solr for village allocation.
    $fields['has_pitches'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Has Pitches'))
      ->setDescription(t('Whether this group has bookings that require pitch allocation'))
      ->setComputed(TRUE)
      ->setClass(HasPitchesItemList::class);

    // Indexed with solr for village allocation.
    $fields['has_bookings'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Has Bookings'))
      ->setDescription(t('Whether this group has bookings'))
      ->setComputed(TRUE)
      ->setClass(HasBookingsItemList::class);

    // Indexed with solr for village allocation.
    $fields['allocated_villages'] = BaseFieldDefinition::create('integer')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(t('Allocated Villages'))
      ->setDescription(t('Which villages this group is allocated to'))
      ->setComputed(TRUE)
      ->setClass(AllocatedVillagesItemList::class);

    return $fields;
  }

  /**
   * Gets the list of available village group types.
   *
   * @return array
   *   The village group type labels keyed by plugin id.
   */
  public static function getVillageTypes() {
    return array_map(function ($item) {
      return $item['label'];
    }, \Drupal::service('plugin.manager.c_events_village_group_type')->getDefinitions());
  }

  /**
   * Clears the cached value of computed fields.
   */
  public function clearComputedFields() {
    $fields = [
      'has_requirements',
      'has_pitches',
      'has_bookings',
      'allocated_villages',
    ];

    foreach ($fields as $field) {
      // resetValue is not defined on the base class, but is defined
      // individually on each of these custom list classes.
      if (method_exists($this->get($field), 'resetValue')) {
        $this->get($field)->resetValue();
      }
    }
  }

}

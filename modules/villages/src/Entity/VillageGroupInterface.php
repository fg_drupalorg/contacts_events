<?php

namespace Drupal\contacts_events_villages\Entity;

use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Provides an interface for defining Camping village group entities.
 *
 * @ingroup contacts_events_villages
 */
interface VillageGroupInterface extends ContentEntityInterface {

  /**
   * Gets the Camping village group description if bundle supports descriptions.
   *
   * @return string|null
   *   Description of the Camping village group or NULL if none provide.
   */
  public function getDescription();

  /**
   * Sets the Camping village group description if bundle supports descriptions.
   *
   * @param string $description
   *   The Camping village group description.
   *
   * @return \Drupal\contacts_events_villages\Entity\VillageGroupInterface
   *   The called Camping village group entity.
   */
  public function setDescription($description);

  /**
   * Gets the Camping village group name.
   *
   * @return string
   *   Name of the Camping village group.
   */
  public function getName();

  /**
   * Sets the Camping village group name.
   *
   * @param string $name
   *   The Camping village group name.
   *
   * @return \Drupal\contacts_events_villages\Entity\VillageGroupInterface
   *   The called Camping village group entity.
   */
  public function setName($name);

  /**
   * Sets the event for this village group.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface|int $event
   *   Event or event ID.
   *
   * @return \Drupal\contacts_events_villages\Entity\VillageGroupInterface
   *   The called Camping village group entity.
   */
  public function setEvent($event);

  /**
   * Gets the event associated with this village group.
   *
   * @return \Drupal\contacts_events\Entity\Event
   *   The event.
   */
  public function getEvent();

  /**
   * Gets the event id associated with this village group.
   *
   * @return int
   *   The event id.
   */
  public function getEventId();

}

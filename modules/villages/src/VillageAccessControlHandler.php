<?php

namespace Drupal\contacts_events_villages;

use Drupal\contacts_events\Access\EventAccessTrait;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Camping village group entity.
 *
 * @see \Drupal\contacts_events_villages\Entity\VillageGroup.
 */
class VillageAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  use EventAccessTrait;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return (new static($entity_type))
      ->setRouteMatch($container->get('current_route_match'))
      ->setEventStorage($container->get('entity_type.manager')->getStorage('contacts_event'));
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\contacts_events_villages\Entity\Village $entity */
    $event = $entity->getEvent();

    switch ($operation) {
      case 'view label':
        return AccessResult::allowed();

      case 'view':
      case 'update':
      case 'delete':
        // Check can manage villages and also update event.
        return AccessResult::allowedIfHasPermission($account, 'manage c_events_village entities')
          ->andIf(AccessResult::allowedIf($event->access('update', $account)));
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // @todo Should find a way to check the event update access
    // Like we do above for view/update/delete.
    return AccessResult::allowedIfHasPermission($account, 'manage c_events_village entities');
  }

}

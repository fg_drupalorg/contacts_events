<?php

/**
 * @file
 * Provides views execution hooks for contacts_events_villages module.
 */

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_post_execute().
 */
function contacts_events_villages_views_post_execute(ViewExecutable $view) {
  if ($view->id() == 'contacts_events_village_hosts_report') {
    $view->build_info += ['substitutions' => []];
    $view->build_info['substitutions'] += [
      '_totals' => [
        'delegates' => 0,
        'bookings' => 0,
        'camping_groups' => 0,
        'pitches' => 0,
      ],
      '_group_totals' => [],
    ];

    $totals = &$view->build_info['substitutions']['_totals'];
    $group_totals = &$view->build_info['substitutions']['_group_totals'];

    foreach ($view->result as $row_index => $row) {
      $group_id = $view->field['id']->getValue($row);

      if (!isset($group_totals[$group_id])) {
        $totals['camping_groups']++;
        $group_totals[$group_id] = [
          'delegates' => 0,
          'bookings' => 0,
          'pitches' => 0,
        ];
      }

      $totals['bookings']++;
      $group_totals[$group_id]['bookings']++;

      $pitches = $view->field['accommodation']->getValue($row, 'pitches');
      $totals['pitches'] += $pitches;
      $group_totals[$group_id]['pitches'] += $pitches;

      /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
      $order = $row->_entity;
      foreach ($order->getItems() as $item) {
        if ($item->bundle() != 'contacts_ticket') {
          continue;
        }
        if (in_array($item->get('state')->value, ['pending', 'cancelled'])) {
          continue;
        }
        $totals['delegates']++;
        $group_totals[$group_id]['delegates']++;
      }
    }
  }
}

<?php

namespace Drupal\Tests\contacts_events_teams\Kernel;

use Drupal\contacts_dbs\Entity\DBSWorkforce;
use Drupal\contacts_events\Entity\Event;
use Drupal\contacts_events\Entity\Ticket;
use Drupal\contacts_events_teams\Entity\Team;
use Drupal\contacts_events_teams\Entity\TeamApplication;
use Drupal\contacts_events_teams\Entity\TeamInterface;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Test the team application dbs integration.
 *
 * @group contacts_events
 */
class TeamDbsTest extends CommerceKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce',
    'commerce_checkout',
    'commerce_order',
    'commerce_price',
    'commerce_payment',
    'commerce_store',
    'contacts',
    'contacts_dbs',
    'contacts_events',
    'contacts_events_teams',
    'ctools',
    'datetime',
    'datetime_range',
    'decoupled_auth',
    'entity',
    'entity_reference_revisions',
    'facets',
    'field',
    'file',
    'image',
    'name',
    'options',
    'profile',
    'rest',
    'search_api',
    'serialization',
    'state_machine',
    'taxonomy',
    'views',
    'views_data_export',
  ];

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * The dbs status owner.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $user;

  /**
   * The a event being booked for.
   *
   * @var \Drupal\contacts_events\Entity\Event
   */
  protected $event;

  /**
   * The a team being applied to.
   *
   * @var \Drupal\contacts_events_teams\Entity\TeamInterface
   */
  protected $team;

  /**
   * The a team application being submitted.
   *
   * @var \Drupal\contacts_events_teams\Entity\TeamApplication
   */
  protected $app;

  /**
   * The events that were triggered by a transition.
   *
   * @var array
   */
  protected $triggeredEvents = [];

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();
    $this->installEntitySchema('file');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_payment');
    $this->installEntitySchema('contacts_event');
    $this->installEntitySchema('dbs_status');
    $this->installEntitySchema('c_events_team');
    $this->installEntitySchema('c_events_team_app');
    $this->installEntitySchema('contacts_ticket');
    $this->installEntitySchema('profile');
    $this->installConfig('contacts');
    $this->installConfig('commerce_order');
    $this->installConfig('commerce_payment');
    $this->installConfig('contacts_events');
    $this->installConfig('contacts_events_teams');

    $contacts_events_config = $this->container->get('config.factory')->getEditable('contacts_events.booking_settings');
    $contacts_events_config->set('store_id', $this->store->id())->save();

    DBSWorkforce::create([
      'id' => 'default',
      'valid' => 1,
      'alternatives' => [],
    ])->save();

    DBSWorkforce::create([
      'id' => 'other',
      'valid' => 1,
      'alternatives' => [],
    ])->save();

    $this->user = $this->createUser();
    $this->user->save();

    // Create our events.
    $event = Event::create([
      'type' => 'default',
      'title' => 'Default',
      'settings' => [
        'teams' => ['enabled' => TeamInterface::STATUS_OPEN],
      ],
    ]);
    $event->save();

    // Create our events.
    $this->team = Team::create([
      'name' => 'Default team',
      'event' => $event->id(),
    ]);
    $this->team->save();

    $ticket = Ticket::create([
      'type' => 'contacts_ticket',
      'event' => $event->id(),
      'team' => $this->team->id(),
    ]);
    $ticket->save();

    $this->app = TeamApplication::create([
      'type' => 'default',
      'team' => $this->team->id(),
      'ticket' => $ticket->id(),
      'event' => $event->id(),
      'user_id' => $this->user->id(),
    ]);
    $this->app->save();

    $dispatcher = $this->container->get('event_dispatcher');
    $phases = [
      'pre_transition',
      'post_transition',
    ];

    $events = [];
    foreach ($phases as $phase) {
      $events[] = "contacts_events_teams_applications.submit.{$phase}";
      $events[] = "contacts_events_teams_applications.{$phase}";
      $events[] = "state_machine.{$phase}";
    }

    foreach ($events as $event_name) {
      $dispatcher->addListener($event_name, function () use ($event_name) {
        $this->trackEvent($event_name);
      });
    }
  }

  /**
   * Test checks dbs status triggers for submitting a team application.
   *
   * @param array|false $create_dbs
   *   Data to be used to create an existing dbs status.
   * @param string|false $team_workforce
   *   The team workforce for the team being applied to.
   * @param bool $expected_exists
   *   If we expect there to be a dbs status relevant for the team application.
   * @param bool $expected_created
   *   If we had to create a new dbs status on team application submission.
   * @param string|false $expected_status
   *   The expected status of the relevant dbs status, if any.
   *
   * @dataProvider dataTeamApplicationSubmission
   */
  public function testTeamApplicationSubmission($create_dbs, $team_workforce, $expected_exists, $expected_created, $expected_status) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $this->container->get('entity_type.manager');
    /** @var \Drupal\contacts_dbs\DBSStatusStorage $dbs_status_storage */
    $dbs_status_storage = $entity_type_manager->getStorage('dbs_status');
    /** @var \Drupal\contacts_dbs\DBSManager $dbs_manager */
    $dbs_manager = $this->container->get('contacts_dbs.dbs_manager');

    if ($create_dbs) {
      $workforce = $create_dbs['workforce'];
      $status = $create_dbs['status'];
      $create_dbs = $dbs_status_storage->create([
        'workforce' => $workforce,
        'status' => $status,
        'uid' => $this->user->id(),
      ]);
      $create_dbs->save();
    }

    $this->team->set('dbs_workforce', $team_workforce);
    $this->team->save();

    $this->triggeredEvents = [];

    $this->app->get('state')->first()->applyTransitionById('submit');
    $this->app->save();

    // Test the main transition events triggered.
    static::assertTrue(in_array("contacts_events_teams_applications.submit.pre_transition", $this->triggeredEvents), 'Main transition pre_transition did not happen.');
    static::assertTrue(in_array("contacts_events_teams_applications.submit.post_transition", $this->triggeredEvents), 'Main transition post_transition did not happen.');

    // Get an existing relevant dbs status.
    $status = $dbs_manager->getDbs($this->user->id(), 'default');

    // Check that the dbs lookup found a status.
    if ($expected_exists) {
      static::assertNotNull($status);
    }
    else {
      static::assertNull($status);
    }

    if ($create_dbs && $expected_created) {
      static::assertNotEquals($status->id(), $create_dbs->id());
    }
    elseif ($create_dbs) {
      static::assertEquals($status->id(), $create_dbs->id());
    }

    if ($expected_status) {
      static::assertEquals($expected_status, $status->get('status')->value);
    }
  }

  /**
   * Data provider for testTeamApplicationSubmission.
   */
  public function dataTeamApplicationSubmission() {
    $data['no_dbs_non_dbs_team'] = [
      'create_dbs' => FALSE,
      'team_workforce' => FALSE,
      'expected_exists' => FALSE,
      'expected_created' => FALSE,
      'expected_status' => FALSE,
    ];

    $data['no_dbs_with_dbs_team'] = [
      'create_dbs' => FALSE,
      'team_workforce' => 'default',
      'expected_exists' => TRUE,
      'expected_created' => TRUE,
      'expected_status' => 'letter_required',
    ];

    $data['valid_dbs_with_dbs_team'] = [
      'create_dbs' => [
        'status' => 'dbs_clear',
        'workforce' => 'default',
      ],
      'team_workforce' => 'default',
      'expected_exists' => TRUE,
      'expected_created' => FALSE,
      'expected_status' => 'dbs_clear',
    ];

    $data['invalid_dbs_with_dbs_team'] = [
      'create_dbs' => [
        'status' => NULL,
        'workforce' => 'other',
      ],
      'team_workforce' => 'default',
      'expected_exists' => TRUE,
      'expected_created' => TRUE,
      'expected_status' => 'letter_required',
    ];

    $data['expired_dbs_with_dbs_team'] = [
      'create_dbs' => [
        'status' => 'dbs_expired',
        'workforce' => 'default',
      ],
      'team_workforce' => 'default',
      'expected_exists' => TRUE,
      'expected_created' => FALSE,
      'expected_status' => 'letter_required',
    ];

    return $data;
  }

  /**
   * Track which transition events have been triggered.
   *
   * @param string $event_name
   *   The name of the triggered event.
   */
  public function trackEvent($event_name) {
    $this->triggeredEvents[$event_name] = $event_name;
  }

}

<?php

namespace Drupal\contacts_events_teams\Plugin\Block;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\contacts_events_teams\TeamQueries;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a block to view a contact dashboard summary.
 *
 * @Block(
 *   id = "contacts_events_teams",
 *   admin_label = @Translation("Contact Teams"),
 *   category = @Translation("Dashboard Blocks"),
 *   dashboard_block = TRUE,
 *   context_definitions = {
 *     "user" = @ContextDefinition("entity:user", required = TRUE, label = @Translation("User"))
 *   }
 * )
 */
class ContactsDashboardTeams extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The team application entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * The time service.
   *
   * @var \DateTimeInterface
   */
  protected $time;

  /**
   * The team queries service.
   *
   * @var \Drupal\contacts_events_teams\TeamQueries
   */
  protected $queries;

  /**
   * Construct the teams block.
   *
   * @param array $configuration
   *   The plugin configuration.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The team application entity storage.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\contacts_events_teams\TeamQueries $queries
   *   The team queries service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityStorageInterface $storage, TimeInterface $time, TeamQueries $queries) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->storage = $storage;
    $this->time = $time;
    $this->queries = $queries;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager')->getStorage('contacts_ticket'),
      $container->get('datetime.time'),
      $container->get('contacts_events_teams.queries')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\user\Entity\User $user */
    $user = $this->getContextValue('user');

    $cut_off = date(DateTimeItemInterface::DATETIME_STORAGE_FORMAT, $this->time->getRequestTime());

    // Upcoming events first.
    $query = $this->storage->getQuery();
    $query->accessCheck();
    $query->condition('contact', $user->id());
    $query->condition('is_team_ticket', TRUE);
    $query->condition('event.entity.date.end_value', $cut_off, '>=');
    $query->sort('event.entity.date.value', 'DESC');
    $ids = $query->execute();
    $build['upcoming'] = $this->buildAppTable($ids);

    // Past events second.
    $query = $this->storage->getQuery();
    $query->accessCheck();
    $query->condition('contact', $user->id());
    $query->condition('is_team_ticket', TRUE);
    $query->condition('event.entity.date.end_value', $cut_off, '<');
    $query->sort('event.entity.date.value', 'DESC');
    $ids = $query->execute();
    if ($ids) {
      $build['past_title'] = [
        '#type' => 'html_tag',
        '#tag' => 'h3',
        '#value' => $this->t('Past events'),
      ];
      $build['past'] = $this->buildAppTable($ids);
    }

    return $build;
  }

  /**
   * Build the application table for a set of IDs.
   *
   * @param array $ids
   *   The application IDs for the table.
   *
   * @return array
   *   The render array.
   */
  protected function buildAppTable(array $ids) {
    $build = [
      '#type' => 'table',
      '#attributes' => ['class' => ['table', 'table-hover', 'table-striped']],
      '#header' => [
        'event' => $this->t('Event'),
        'team' => $this->t('Team'),
        'status' => $this->t('Status'),
        'view' => $this->t('View'),
        'edit' => $this->t('Edit'),
        'operations' => $this->t('Operations'),
      ],
      '#rows' => [],
      '#empty' => $this->t('No team applications'),
    ];

    /** @var \Drupal\contacts_events\Entity\TicketInterface $ticket */
    foreach ($this->storage->loadMultiple($ids) as $ticket) {
      /** @var \Drupal\contacts_events\Entity\EventInterface $event */
      $event = $ticket->get('event')->entity;
      $app = $this->queries->getTeamApplicationForTicket($ticket);

      $row = [];
      $row['event'] = ['#markup' => $event->label()];
      $row['team'] = [
        '#markup' => $ticket->get('team')->entity ? $ticket->get('team')->entity->label() : $this->t('Missing Team'),
      ];
      $row['status'] = [
        '#markup' => $app ? $app->state->first()->getLabel() : $this->t('Not started'),
      ];
      $row['view'] = [
        '#markup' => $app ? $app->toLink($this->t('View Application'))->toString() : '',
      ];
      $row['edit'] = [
        '#markup' => $app ? $app->toLink($this->t('Edit'), 'edit-form')->toString() : '',
      ];
      $row['operations'] = $app ?
        $app->get('state')->view([
          'type' => 'state_transition_form',
          'label' => 'hidden',
        ]) :
        ['#markup' => ''];
      $build[$ticket->id()] = $row;
    }

    return $build;
  }

}

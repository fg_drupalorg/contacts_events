<?php

namespace Drupal\contacts_events_teams\Plugin\views\field;

use Drupal\Core\Entity\EntityInterface;
use Drupal\views\Plugin\views\field\EntityOperations as CoreEntityOperations;
use Drupal\views\ResultRow;

/**
 * An extension of core EntityOperations.
 *
 * This is to get round changes that means a patch cannot apply both supported
 * branches due to switching from entity manager to entity type manager.
 *
 * @todo Remove when Drupal 8.6 is no longer supported.
 *
 * @see: https://www.drupal.org/project/drupal/issues/2916597
 */
class EntityOperations extends CoreEntityOperations {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntity($values);
    if (!($entity instanceof EntityInterface)) {
      return;
    }
    $entity = $this->getEntityTranslation($entity, $values);
    $operations = $this->entityTypeManager->getListBuilder($entity->getEntityTypeId())->getOperations($entity);

    if ($this->options['destination']) {
      foreach ($operations as &$operation) {
        if (!isset($operation['query'])) {
          $operation['query'] = [];
        }
        $operation['query'] += $this->getDestinationArray();
      }
    }
    $build = [
      '#type' => 'operations',
      '#links' => $operations,
    ];

    return $build;
  }

}

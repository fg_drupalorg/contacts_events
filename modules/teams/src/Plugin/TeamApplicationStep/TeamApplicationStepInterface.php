<?php

namespace Drupal\contacts_events_teams\Plugin\TeamApplicationStep;

use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\Core\Form\FormInterface;

/**
 * Team application step.
 */
interface TeamApplicationStepInterface extends FormInterface {

  /**
   * Set the ticket the application is for.
   *
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket.
   *
   * @return $this
   */
  public function setTicket(TicketInterface $ticket);

  /**
   * Set the configuration for the form.
   *
   * @param array $configuration
   *   The configuration.
   *
   * @return $this
   */
  public function setConfiguration(array $configuration);

}

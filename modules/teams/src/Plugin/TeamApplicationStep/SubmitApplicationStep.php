<?php

namespace Drupal\contacts_events_teams\Plugin\TeamApplicationStep;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Submit step.
 *
 * @TeamApplicationStep(
 *   id = "submit",
 *   label = @Translation("Submit application"),
 *   weight = 100,
 * )
 */
class SubmitApplicationStep extends FormBase implements TeamApplicationStepInterface {

  use TeamStepsTrait;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return parent::create($container)
      ->setQueries($container->get('contacts_events_teams.queries'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_events_teams_submit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['header'] = [
      '#type' => 'html_tag',
      '#tag' => 'h3',
      '#value' => $this->t('Failure to disclose information which subsequently comes to light could result in your being asked to leave the site.'),
    ];

    $form['list'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => [
        $this->t('The information contained is accurate to the best of my knowledge.'),
        $this->t('I consent to references and further checks being made with the relevant authorities if necessary.'),
        $this->t('You must have permission from your referee before sharing their email address.'),
        $this->t('I agree that, as a volunteer, my primary commitment is to my team.'),
      ],
    ];

    $form['footer'] = [
      '#markup' => $this->t('Once you submit your application, you will not be able to change any details without calling the office. Please check you have filled everything in before submitting your application.'),
    ];

    $form['actions'] = $this->actionsElement($form, $form_state);
    // Rename "Next" to "Submit".
    $form['actions']['submit']['#value'] = $this->t('Submit Application');
    // Don't need the Save and Next handlers as this is the last step.
    $form['actions']['submit']['#submit'] = ['::submitForm'];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Transition the team app.
    // The ticket is automatically transitioned to team app submitted state
    // by the TeamTicketStateSubscriber.
    $app = $this->getTeamApplication();
    $app->state->first()->applyTransitionById('submit');
    $app->save();

    $this->messenger()->addMessage('Thank you for your application. You should expect to receive an email once your application has been approved.');
    $form_state->setRedirect('user.page');
  }

}

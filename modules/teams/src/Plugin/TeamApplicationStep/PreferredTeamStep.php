<?php

namespace Drupal\contacts_events_teams\Plugin\TeamApplicationStep;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Select preferred team step.
 *
 * @TeamApplicationStep(
 *   id = "team",
 *   label = @Translation("Preferred team"),
 *   weight = 2,
 * )
 */
class PreferredTeamStep extends FormBase implements TeamApplicationStepInterface {

  use TeamStepsTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Team entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $teamStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->teamStorage = $entity_type_manager->getStorage('c_events_team');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_events_teams_select_team_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $ticket = $this->getTicket();

    // @todo Make all of this configurable.
    if ($ticket->team->target_id) {
      $form['already_chosen'] = [
        '#markup' => $this->t('<strong>You are applying for:</strong> @team<br/><br/>If you wish to change the team you are applying for, please contact the office.', ['@team' => $ticket->team->entity->label()]),
      ];
    }
    else {
      $query = $this->teamStorage->getQuery();
      $query->accessCheck();
      $query->condition('event', $ticket->event->target_id);
      $query->condition('public', TRUE);
      /** @var \Drupal\contacts_events_teams\Entity\Team[] $teams */
      $teams = $this->teamStorage->loadMultiple($query->execute());

      foreach ($teams as $team) {
        $options[$team->id()] = $team->label();
      }

      $form['teams'] = [
        '#title' => $this->t("Please select the team you're applying for."),
        '#type' => 'select',
        '#options' => $options,
        '#required' => TRUE,
      ];
    }

    $form['actions'] = $this->actionsElement($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
  }

}

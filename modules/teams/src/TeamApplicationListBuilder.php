<?php

namespace Drupal\contacts_events_teams;

use Drupal\contacts_events\Entity\EventClass;
use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a class to build a listing of Team application entities.
 *
 * @ingroup contacts_events_teams
 */
class TeamApplicationListBuilder extends EntityListBuilder {

  /**
   * The Event Class entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $classStorage;

  /**
   * An array of class labels.
   *
   * @var string[]
   */
  protected $classLabels;

  /**
   * Current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Constructs a new EntityListBuilder object.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type definition.
   * @param \Drupal\Core\Entity\EntityStorageInterface $storage
   *   The entity storage class.
   * @param \Drupal\Core\Entity\EntityStorageInterface $class_storage
   *   The Event Class entity storage class.
   * @param \Drupal\Core\Routing\CurrentRouteMatch $route_match
   *   The current route match.
   */
  public function __construct(EntityTypeInterface $entity_type, EntityStorageInterface $storage, EntityStorageInterface $class_storage, CurrentRouteMatch $route_match) {
    parent::__construct($entity_type, $storage);
    $this->classStorage = $class_storage;
    $this->routeMatch = $route_match;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    /** @var \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager */
    $entity_type_manager = $container->get('entity_type.manager');
    return new static(
      $entity_type,
      $entity_type_manager->getStorage($entity_type->id()),
      $entity_type_manager->getStorage('contacts_events_class'),
      $container->get('current_route_match')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render() {
    $build = parent::render();

    /** @var \Drupal\contacts_events\Entity\Event $event */
    $event = $this->routeMatch->getParameter('contacts_event');
    /** @var \Drupal\contacts_events_teams\Entity\Team $team */
    $team = $this->routeMatch->getParameter('c_events_team');

    $params = [
      '@collection_label' => $this->entityType->getCollectionLabel(),
      '@event' => $event->label(),
    ];
    if ($team) {
      $params['@team'] = $team->label();
      $build['#title'] = $this->t('@collection_label for @team at @event', $params);
    }
    else {
      $build['#title'] = $this->t('@collection_label for @event', $params);
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = [
      'applicant' => $this->t('Applicant'),
      'ticket_class' => $this->t('Ticket class'),
      'team' => $this->t('Team'),
      'status' => $this->t('Status'),
    ] + parent::buildHeader();
    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /** @var \Drupal\contacts_events_teams\Entity\TeamApplication $entity */
    $row = [];
    $row['applicant'] = Link::createFromRoute($entity->getOwner()->label(),
      'contacts.contact',
      ['user' => $entity->getOwnerId()]);
    $row['ticket_class'] = $this->getClassLabel($entity->getTicket());
    $row['team'] = $entity->getTeam() ? $entity->getTeam()->toLink() : new TranslatableMarkup('Team has been deleted');
    $row['status'] = $entity->get('state')->first()->getLabel();
    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultOperations(EntityInterface $entity) {
    $operations = [];

    /** @var \Drupal\contacts_events_teams\Entity\TeamApplication $entity */
    if ($entity->access('view')) {
      $operations['view'] = [
        'title' => $this->t('View Application'),
        'url' => $entity->toUrl('canonical'),
      ];
    }

    return $operations;
  }

  /**
   * Get the event class label for a ticket.
   *
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket to retrieve for.
   *
   * @return string
   *   The class label.
   */
  protected function getClassLabel(TicketInterface $ticket) {
    if (!isset($this->classLabels)) {
      $this->classLabels = array_map(function (EventClass $class) {
        return $class->label();
      }, $this->classStorage->loadMultiple());
    }
    $class = $ticket->getMappedPrice()['class'];
    return $this->classLabels[$class] ?? '-';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEntityIds() {
    $event = $this->routeMatch->getRawParameter('contacts_event');
    $team = $this->routeMatch->getRawParameter('c_events_team');

    $query = $this->getStorage()->getQuery()
      ->sort($this->entityType->getKey('id'));
    $query->accessCheck();

    $query->condition('event', $event);
    if ($team) {
      $query->condition('team', $team);
    }

    // Only add the pager if a limit is specified.
    if ($this->limit) {
      $query->pager($this->limit);
    }
    return $query->execute();
  }

}

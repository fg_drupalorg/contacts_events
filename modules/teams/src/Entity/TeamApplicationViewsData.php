<?php

namespace Drupal\contacts_events_teams\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Team application entities.
 */
class TeamApplicationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    $data['c_events_team_app']['state']['filter']['id'] = 'state_machine_state';
    $data['c_events_team_app']['team']['filter']['id'] = 'contacts_events_teams_team';

    $data['c_events_team_app']['confirmed_date'] = [
      'title' => $this->t('Confirmed Date'),
      'field' => [
        'id' => 'contacts_events_teams_team_application_confirmed_date',
      ],
    ];
    return $data;
  }

}

<?php

namespace Drupal\contacts_events_teams\Entity;

use Drupal\contacts_events\Entity\Ticket;
use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Team application entity.
 *
 * @ingroup contacts_events_teams
 *
 * @ContentEntityType(
 *   id = "c_events_team_app",
 *   label = @Translation("Team application"),
 *   label_plural = @Translation("Team applications"),
 *   label_collection = @Translation("Team applications"),
 *   bundle_label = @Translation("Team application form"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contacts_events_teams\TeamApplicationListBuilder",
 *     "views_data" = "Drupal\contacts_events_teams\Entity\TeamApplicationViewsData",
 *     "form" = {
 *       "default" = "Drupal\contacts_events_teams\Form\TeamApplicationForm",
 *       "edit" = "Drupal\contacts_events_teams\Form\TeamApplicationForm",
 *       "delete" = "Drupal\contacts_events_teams\Form\TeamApplicationDeleteForm",
 *     },
 *     "access" = "Drupal\contacts_events_teams\TeamApplicationAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_events_teams\TeamApplicationHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "c_events_team_app",
 *   admin_permission = "administer team application entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *   },
 *   links = {
 *     "collection" = "/event/{contacts_event}/teams/applications",
 *     "canonical" = "/event/{contacts_event}/teams/{c_events_team}/applications/{c_events_team_app}",
 *     "edit-form" = "/event/{contacts_event}/teams/{c_events_team}/applications/{c_events_team_app}/edit"
 *   },
 *   bundle_entity_type = "c_events_team_app_type",
 *   field_ui_base_route = "entity.c_events_team_app_type.edit_form"
 * )
 */
class TeamApplication extends ContentEntityBase implements EntityOwnerInterface {

  use EntityChangedTrait;

  /**
   * Creates a new team application, for the specified team and ticket.
   *
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket that will be associated with the team application.
   * @param \Drupal\contacts_events_teams\Entity\TeamInterface $team
   *   The team that the applicant is applying for.
   * @param bool $auto_accept
   *   Whether the application should be automatically accepted.
   *   Defaults to FALSE.
   *
   * @return \Drupal\contacts_events_teams\Entity\TeamApplication
   *   The new application.
   */
  public static function createForTicket(TicketInterface $ticket, TeamInterface $team, $auto_accept = FALSE) {
    $application = TeamApplication::create([
      'type' => $team->get('form')->target_id,
      'user_id' => $ticket->getTicketHolderId(),
      'team' => $team->id(),
      'ticket' => $ticket->id(),
      'event' => $ticket->getEvent()->id(),
      'state' => $auto_accept ? 'approved' : 'draft',
    ]);

    return $application;
  }

  /**
   * Changes the team that an application is for.
   *
   * If the new team has the same application form as the old team,
   * this application will be updated with the new team, saved and returned.
   *
   * If the new team has a different application form, a new application will
   * be created, field values copied across (where they can), saved and
   * returned. This application will be archived and saved.
   *
   * @param \Drupal\contacts_events_teams\Entity\TeamApplication $existing_application
   *   The application for which the team to be changed.
   * @param \Drupal\contacts_events_teams\Entity\Team $team
   *   The team to change to.
   * @param bool $auto_accept
   *   Whether the new application form should be automatically accepted
   *   (TRUE) or needs to be filled in by the applicant (FALSE).
   *   Defaults to FALSE.
   *
   * @return \Drupal\contacts_events_teams\Entity\TeamApplication
   *   Either this application with the team updated, or a new team application.
   */
  public static function changeTeam(TeamApplication $existing_application, Team $team, $auto_accept = FALSE) {
    $application_status = $auto_accept ? 'approved' : 'draft';

    // If the two teams use the same application form, just change this
    // application's team and reset its status to either draft/approved.
    if ($existing_application->bundle() == $team->get('form')->target_id) {
      $existing_application->set('team', $team->id());
      $existing_application->set('state', $application_status);
      $existing_application->save();
      return $existing_application;
    }
    else {
      // If the two teams *don't* use the same application form, create
      // a new application.
      $new_application = $existing_application->cloneWithDifferentTeam($team);
      $new_application->set('state', $application_status);
      $new_application->save();

      // Archive old form.
      $existing_application->set('state', 'archived');
      // Flag that we're in the middle of a transfer process.
      // The TeamTicketStateSubscriber will normally reset a ticket back to
      // pending when an application is archived. Setting a surrogate property
      // on the application will prevent this
      // (see TeamTicketStateSubscriber::updateTicketState)
      $existing_application->is_transferring = TRUE;
      $existing_application->save();

      return $new_application;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
      'state' => 'draft',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * Whether the team application has been submitted.
   *
   * @return bool
   *   Whether the team application has been submitted.
   */
  public function isSubmitted() : bool {
    // If it's not draft, it's been submitted.
    return $this->state->value && $this->state->value != 'draft';
  }

  /**
   * Gets the team for this application.
   *
   * @return \Drupal\contacts_events_teams\Entity\Team|null
   *   The team.
   */
  public function getTeam() : ?Team {
    return $this->get('team')->entity;
  }

  /**
   * The ticket associated with this team application.
   *
   * @return \Drupal\contacts_events\Entity\Ticket|null
   *   The ticket associated with this team application.
   */
  public function getTicket() : ?Ticket {
    return $this->get('ticket')->entity;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $parameters = parent::urlRouteParameters($rel);
    $parameters['c_events_team'] = $this->get('team')->target_id;
    $parameters['contacts_event'] = $this->get('event')->target_id;
    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['state'] = BaseFieldDefinition::create('state')
      ->setLabel(t('Application Status'))
      ->setDescription(t('The team application state.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setSetting('workflow', 'contacts_events_teams_application_process')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'state_transition_form',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['team'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Team'))
      ->setSetting('target_type', 'c_events_team')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['ticket'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Ticket'))
      ->setDescription(t('Ticket'))
      ->setSetting('target_type', 'contacts_ticket')
      ->setSetting('handler', 'default')
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['event'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Event'))
      ->setDescription(t('The Event'))
      ->setSetting('target_type', 'contacts_event')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User'))
      ->setDescription(t('The user ID of author of the Team application entity.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['status_log'] = BaseFieldDefinition::create('status_log')
      ->setLabel(t('Status History'))
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setDescription(t('A log of when the status was changed and by whom.'))
      ->setSettings([
        'max_length' => 50,
        'source_field' => 'state',
      ])
      ->setDisplayOptions('view', [
        'weight' => 20,
        'type' => 'status_log_list',
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    return $fields;
  }

  /**
   * Retreives a collection of transition IDs that the user can apply manually.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The account.
   *
   * @return array
   *   Array of transition IDs that the user can invoke.
   */
  public function getManuallyAllowedTransitions(AccountInterface $account) {
    /** @var \Drupal\state_machine\Plugin\Field\FieldType\StateItem $state */
    $state = $this->state->first();
    $transitions = array_keys($state->getTransitions());

    $staff_only_transitions = [
      'submit',
      'accept',
      'approve',
      'reject',
      'reject_notify',
      'archive',
      'reset_to_not_submitted',
    ];

    if ($account->hasPermission('manage all contacts events team applications')) {
      return array_intersect($transitions, $staff_only_transitions);
    }

    $team_leader_transitions = [
      'accept',
      'reject',
    ];

    if ($this->getTeam() && $this->getTeam()->isTeamLeader($account->id())) {
      return array_intersect($transitions, $team_leader_transitions);
    }

    return [];
  }

  /**
   * Creates a new application based on an existing one, for a different team.
   *
   * Sets up the new application for the same ticketholder as the other
   * application, but for a different team.
   *
   * Copies application questions and references (defined as config fields)
   * across where both applications have the same fields (they may not, if the
   * team has different application forms that have different questions
   * defined).
   *
   * @param \Drupal\contacts_events_teams\Entity\Team $team
   *   The team that the applicant is applying for.
   *
   * @return \Drupal\contacts_events_teams\Entity\TeamApplication
   *   The new team application.
   */
  protected function cloneWithDifferentTeam(Team $team) {
    $ticket = $this->getTicket();
    $application = TeamApplication::createForTicket($ticket, $team);

    // Find all config fields on the original team application. If the new
    // application has a field with the same name, copy the value across.
    // Only want to do this for config fields (ie the fields that have
    // the questions defined).
    foreach ($this->getFields(FALSE) as $field) {
      if ($field->getFieldDefinition() instanceof FieldConfig && $application->hasField($field->getName())) {
        $application->set($field->getName(), $field->getValue());
      }
    }

    return $application;
  }

}

<?php

namespace Drupal\contacts_events_teams\Entity;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\entity\BundleFieldDefinition;

/**
 * Defines the Team entity.
 *
 * @ingroup contacts_events_teams
 *
 * @ContentEntityType(
 *   id = "c_events_team",
 *   label = @Translation("Team"),
 *   label_plural = @Translation("Teams"),
 *   label_collection = @Translation("Teams"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contacts_events_teams\TeamListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\contacts_events_teams\Form\TeamForm",
 *       "edit" = "Drupal\contacts_events_teams\Form\TeamForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *     },
 *     "access" = "Drupal\contacts_events_teams\TeamAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_events_teams\TeamHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "c_events_team",
 *   admin_permission = "administer contacts events teams",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode",
 *   },
 *   links = {
 *     "canonical" = "/event/{contacts_event}/teams/{c_events_team}",
 *     "edit-form" = "/event/{contacts_event}/teams/{c_events_team}/edit",
 *     "add-form" = "/event/{contacts_event}/teams/add",
 *     "delete-form" = "/event/{contacts_event}/teams/{c_events_team}/delete",
 *     "collection" = "/event/{contacts_event}/teams/manage",
 *   }
 * )
 */
class Team extends ContentEntityBase implements TeamInterface {

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEvent() : EventInterface {
    return $this->get('event')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setEvent($event) {
    $this->set('event', $event);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isTeamLeader($user) : bool {
    if ($user instanceof AccountInterface) {
      $user = $user->id();
    }

    foreach ($this->get('leaders') as $team_leader_item) {
      if ($team_leader_item->target_id == $user) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $parameters = parent::urlRouteParameters($rel);
    $parameters['contacts_event'] = $this->get('event')->target_id;
    return $parameters;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Team Name'))
      ->setSettings([
        'max_length' => 50,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['event'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Event'))
      ->setDescription(new TranslatableMarkup('The Event'))
      ->setSetting('target_type', 'contacts_event')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 1,
      ])
      ->setDisplayOptions('form', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['public'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Is this team publicly visible?'))
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => 2,
      ])
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['form'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Application Form'))
      ->setDescription(new TranslatableMarkup('Select the application form to be used.'))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'c_events_team_app_type')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    // Translatable strings should have leading/trailing whitespace, but we need
    // that for rendered output, so wrap in formattable markup.
    $age_suffix = new FormattableMarkup(' @year', [
      '@year' => new TranslatableMarkup('year(s)'),
    ]);
    $fields['age_min'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Minimum age'))
      ->setDescription(new TranslatableMarkup('The minimum age for team applicants.'))
      ->setRequired(TRUE)
      ->setDefaultValue(18)
      ->setSetting('suffix', $age_suffix)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => 4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['age_max'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Maximum age'))
      ->setDescription(new TranslatableMarkup('The maximum age for team applicants.'))
      ->setRequired(FALSE)
      ->setDefaultValue(NULL)
      ->setSetting('suffix', $age_suffix)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['leaders'] = BaseFieldDefinition::create('entity_reference')
      ->setCardinality(FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED)
      ->setLabel(new TranslatableMarkup('Team Leaders'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'search_api')
      ->setSetting('handler_settings', [
        'index' => 'contacts_index',
        'conditions' => [['roles', 'crm_indiv']],
      ])
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 6,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 6,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['price_override'] = BaseFieldDefinition::create('commerce_price')
      ->setLabel(new TranslatableMarkup('Price Override'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'commerce_order_total_summary',
        'weight' => 7,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 7,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['category'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Category'))
      ->setSetting('target_type', 'taxonomy_term')
      ->setSetting('handler', 'default')
      ->setSetting('handler_settings', ['target_bundles' => ['contacts_events_teams']])
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 8,
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['capacity'] = BaseFieldDefinition::create('integer')
      ->setLabel(new TranslatableMarkup('Team Capacity'))
      ->setRequired(FALSE)
      ->setDefaultValue(NULL)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 9,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['disable_automatic_emails'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Disable automatic emails for this team'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['team_email_override'] = BaseFieldDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Override team emails'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public static function bundleFieldDefinitions(EntityTypeInterface $entity_type, $bundle, array $base_field_definitions) {
    $fields = parent::bundleFieldDefinitions($entity_type, $bundle, $base_field_definitions);

    $fields['application_request_team_enabled'] = BundleFieldDefinition::create('boolean')
      ->setName('application_request_team_enabled')
      ->setLabel(new TranslatableMarkup('Enable application request (with team)?'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_request_team_b_man'] = BundleFieldDefinition::create('boolean')
      ->setName('application_request_team_b_man')
      ->setLabel(new TranslatableMarkup('Do not send to the booking manager'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_request_team_subject'] = BundleFieldDefinition::create('string')
      ->setName('application_request_team_subject')
      ->setLabel(new TranslatableMarkup('Application request subject (with team)'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_request_team_body'] = BundleFieldDefinition::create('text_long')
      ->setName('application_request_team_body')
      ->setLabel(new TranslatableMarkup('Application request email (with team)'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_accepted_enabled'] = BundleFieldDefinition::create('boolean')
      ->setName('application_accepted_enabled')
      ->setLabel(new TranslatableMarkup('Enable application accepted?'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_accepted_subject'] = BundleFieldDefinition::create('string')
      ->setName('application_accepted_subject')
      ->setLabel(new TranslatableMarkup('Application accepted subject'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_accepted_body'] = BundleFieldDefinition::create('text_long')
      ->setName('application_accepted_body')
      ->setLabel(new TranslatableMarkup('Application accepted email'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_submitted_enabled'] = BundleFieldDefinition::create('boolean')
      ->setName('application_submitted_enabled')
      ->setLabel(new TranslatableMarkup('Enable application submitted?'))
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_submitted_subject'] = BundleFieldDefinition::create('string')
      ->setName('application_submitted_subject')
      ->setLabel(new TranslatableMarkup('Application submitted subject'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    $fields['application_submitted_body'] = BundleFieldDefinition::create('text_long')
      ->setName('application_submitted_body')
      ->setLabel(new TranslatableMarkup('Application submitted email'))
      ->setRequired(FALSE)
      ->setDisplayOptions('view', [
        'region' => 'hidden',
      ])
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * Gets the team name from an order item.
   *
   * Used as a callback for the ticket list screen.
   * See contacts_events_teams_inline_entity_form_table_fields_alter.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   The order item.
   *
   * @return string
   *   The team name
   */
  public static function getTeamNameFromOrderItem(OrderItemInterface $order_item) {
    if ($order_item->bundle() === 'contacts_ticket') {
      /** @var \Drupal\contacts_events\Entity\Ticket $ticket */
      /** @var \Drupal\contacts_events_teams\Entity\TeamInterface $team */
      if ($team = $order_item->getPurchasedEntity()->team->entity) {
        return $team->getName();
      }
    }
    return NULL;
  }

}

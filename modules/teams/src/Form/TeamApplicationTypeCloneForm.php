<?php

namespace Drupal\contacts_events_teams\Form;

use Drupal\contacts_events_teams\Entity\TeamApplicationType;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Contacts Events form.
 */
class TeamApplicationTypeCloneForm extends TeamApplicationTypeForm {

  /**
   * Entity Field Manager Service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   *   Entity Field Manager service.
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Construct the form.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   Entity Field Manager service.
   */
  public function __construct(EntityFieldManagerInterface $entity_field_manager) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_field.manager')
    );
  }

  /**
   * The original application.
   *
   * @var \Drupal\Core\Entity\EntityInterface|null
   *   The original application.
   */
  protected ?EntityInterface $original = NULL;

  /**
   * Get the title for the form page.
   *
   * @param \Drupal\contacts_events_teams\Entity\TeamApplicationType $team_app
   *   The event being cloned.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The title.
   */
  public function title(TeamApplicationType $team_app) {
    return $this->t('Clone %team_app', [
      '%team_app' => $team_app->label(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function setEntity(EntityInterface $entity) {

    // Keep the original for reference.
    $this->original = $entity;

    // We want to set a duplicate rather than the actual entity.
    return parent::setEntity($entity->createDuplicate());
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['_clone_info'] = [
      '#type' => 'container',
      'message' => [
        '#markup' => $this->t('The name of the form needs to be changed above. The new form will be created when the form is submitted: this may take a little time so please be patient.'),
      ],
      '#attributes' => ['class' => ['messages', 'messages--warning']],
      '#weight' => 99,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Clone team application form');
    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

    $this->messenger()->addWarning($this->t('%label is cloning from %source: there may be a slight delay.', [
      '%label' => $this->entity->label(),
      '%source' => $this->original->label(),
    ]));

    // Create the fields.
    $storage = $this->entityTypeManager->getStorage('field_config');

    foreach ($this->getEntityFields() as $field) {

      /** @var \Drupal\field\FieldConfigInterface $field_config */
      if ($field_config = $storage->load('c_events_team_app' . '.' . $this->original->id() . '.' . $field)) {
        $copy = $field_config->toArray();
        $this->updateCopy($copy);

        /** @var \Drupal\field\FieldConfigInterface $field */
        $field = $storage->create($copy);
        $field->save();

      }
    }

    // Then create the form mode.
    $storage = $this->entityTypeManager->getStorage('entity_form_display');
    /** @var \Drupal\Core\Entity\Entity\EntityFormDisplay $form_mode */
    $form_mode = $storage->load('c_events_team_app.' . $this->original->id() . '.default');
    $copy = $form_mode->toArray();
    $this->updateCopy($copy);
    $form_view = $storage->create($copy);
    $form_view->save();
  }

  /**
   * Update the settings of a copied entity.
   *
   * @param array $copy
   *   An array of settings for the copied entity.
   */
  protected function updateCopy(array &$copy): void {
    unset($copy['uuid']);
    unset($copy['id']);
    unset($copy['dependencies']);
    $copy['bundle'] = $this->entity->id();
  }

  /**
   * Gets the names of fields attached to the original entity.
   *
   * @return array
   *   An array of field machine names.
   */
  protected function getEntityFields(): array {
    $return = [];
    $map = $this->entityFieldManager->getFieldMap();

    if (isset($map['c_events_team_app'])) {
      foreach ($map['c_events_team_app'] as $field => $data) {
        if (isset($data['bundles'])) {
          if (in_array($this->original->id(), $data['bundles'])) {
            if (!in_array($this->entity->id(), $data['bundles'])) {
              $return[$field] = $field;
            }
          }
        }
      }
    }

    return $return;
  }

}

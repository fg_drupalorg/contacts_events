<?php

namespace Drupal\contacts_events_teams\EventSubscriber;

use Drupal\contacts_events\Event\PriceCalculationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Changes ticket price to match the team override price.
 *
 * @package Drupal\contacts_events_teams\EventSubscriber
 */
class PriceCalculationSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      PriceCalculationEvent::NAME => 'calculate',
    ];
  }

  /**
   * Changes ticket price to match the team override price.
   *
   * @param \Drupal\contacts_events\Event\PriceCalculationEvent $event
   *   Event.
   */
  public function calculate(PriceCalculationEvent $event) {
    // We only want to deal with tickets.
    if ($event->getOrderItem()->bundle() != 'contacts_ticket') {
      return;
    }

    /** @var \Drupal\contacts_events\Entity\Ticket $ticket */
    $ticket = $event->getPurchasedEntity();
    if ($team = $ticket->get('team')->entity) {
      /** @var \Drupal\commerce_price\Plugin\Field\FieldType\PriceItem $price_override */
      $price_override = $team->get('price_override');
      if (!$price_override->isEmpty()) {
        $event->setPrice($price_override->first()->toPrice());
      }
    }
  }

}

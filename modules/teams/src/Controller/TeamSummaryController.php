<?php

namespace Drupal\contacts_events_teams\Controller;

use Drupal\contacts_events\Entity\Event;
use Drupal\contacts_events_teams\Entity\TeamApplication;
use Drupal\contacts_events_teams\TeamQueries;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\state_machine\Plugin\Workflow\Workflow;
use Drupal\state_machine\Plugin\Workflow\WorkflowState;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controller for managing team application process.
 *
 * @package Drupal\contacts_events_teams\Controller
 */
class TeamSummaryController extends ControllerBase {

  /**
   * Current route.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * Class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * Team Queries.
   *
   * @var \Drupal\contacts_events_teams\TeamQueries
   */
  protected $queries;

  /**
   * The teams workflow.
   *
   * @var \Drupal\state_machine\Plugin\Workflow\Workflow
   */
  protected $workflow;

  /**
   * TeamSummaryController constructor.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   Current route.
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $class_resolver
   *   Class resolver.
   * @param \Drupal\contacts_events_teams\TeamQueries $queries
   *   Team Queries.
   * @param \Drupal\state_machine\Plugin\Workflow\Workflow $workflow
   *   The teams workflow.
   */
  public function __construct(RouteMatchInterface $route_match, ClassResolverInterface $class_resolver, TeamQueries $queries, Workflow $workflow) {
    $this->routeMatch = $route_match;
    $this->classResolver = $class_resolver;
    $this->queries = $queries;
    $this->workflow = $workflow;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('class_resolver'),
      $container->get('contacts_events_teams.queries'),
      $container->get('plugin.manager.workflow')->createInstance('contacts_events_teams_application_process')
    );
  }

  /**
   * Displays the manage team application page.
   *
   * @param \Drupal\contacts_events_teams\Entity\TeamApplication $contacts_team_application
   *   The team application.
   *
   * @return array
   *   Render array.
   */
  public function manage(TeamApplication $contacts_team_application) {
    $build = [];
    $build['#attached']['library'][] = 'contacts_events_teams/manage_application';

    $build['application'] = $this->entityTypeManager()
      ->getViewBuilder('c_events_team_app')
      ->view($contacts_team_application, 'full');

    return $build;
  }

  /**
   * Team stats summary for event.
   *
   * @param \Drupal\contacts_events\Entity\Event $contacts_event
   *   Event.
   *
   * @return array
   *   Render array.
   */
  public function buildPage(Event $contacts_event) {
    $build = [];
    $build['#title'] = $this->t('Teams for <strong>@event</strong>', [
      '@event' => $contacts_event->label(),
    ]);

    $build['summary'] = $this->buildSummary($contacts_event);

    $build['heading'] = [
      '#type' => 'html_tag',
      '#tag' => 'h2',
      '#value' => $this->t('Applicant summary'),
      '#attributes' => [
        'style' => 'margin:4em 0 2em',
      ],
    ];

    $build['categories'] = $this->buildCategories($contacts_event);
    return $build;
  }

  /**
   * Build the team summary.
   *
   * @param \Drupal\contacts_events\Entity\Event $event
   *   The event.
   *
   * @return array
   *   The render array for the summary.
   */
  public function buildSummary(Event $event) {
    $stats = $this->queries->getTeamCounts($event);

    return [
      '#type' => 'table',
      '#header' => [
        'public' => $this->t('Public'),
        'private' => $this->t('Private'),
        'total' => $this->t('Total'),
      ],
      '#rows' => [
        [
          'public' => $stats['public'],
          'private' => $stats['private'],
          'total' => [
            'header' => TRUE,
            'data' => ['#markup' => $stats['total']],
          ],
        ],
      ],
    ];
  }

  /**
   * Build the category overview table.
   *
   * @param \Drupal\contacts_events\Entity\Event $event
   *   The event.
   *
   * @return array
   *   The render array for the tables.
   */
  public function buildCategories(Event $event) {
    $build = [];

    // Get the states and our default empty row.
    $states = array_map(function (WorkflowState $state) {
      return $state->getLabel();
    }, $this->workflow->getStates());
    unset($states['archived']);
    $states = ['not_started' => $this->t('Not started')] + $states;

    $header = ['category' => $this->t('Category')] +
      $states +
      ['total' => $this->t('Total')];
    $default_row = [
      'category' => [
        'header' => TRUE,
        'data' => ['#markup' => ''],
      ],
    ];
    $default_row += array_fill_keys(array_keys($states), 0);
    $default_row['total'] = [
      'header' => TRUE,
      'data' => ['#markup' => 0],
    ];

    // Load our categories.
    /** @var \Drupal\taxonomy\TermInterface[] $categories */
    $categories = $this->entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadTree('contacts_events_teams', 0, 2, TRUE);

    $stats = $this->queries->getTeamAppStatsBySubCategory($event);

    // Build our tables.
    foreach ($categories as $category) {
      $tid = $category->id();
      $stat_rows = [];

      // Top level categories.
      if ($category->parents[0] == 0) {
        $build["{$tid}_header"] = [
          '#type' => 'html_tag',
          '#tag' => 'h3',
          '#value' => $category->label(),
          '#weight' => $category->getWeight(),
          '#attributes' => ['style' => 'margin-top:3em'],
        ];
        $build[$tid] = [
          '#type' => 'table',
          '#header' => $header,
          '#rows' => [],
          '#footer' => [$default_row],
          '#weight' => $category->getWeight() + 0.1,
        ];
        $build[$tid]['#footer'][0]['category']['data']['#markup'] = $this->t('Total');
        $stat_rows[] = &$build[$tid]['#footer'][0];
      }
      // Otherwise put as a row in the correct category.
      else {
        $build[$category->parents[0]]['#rows'][$tid] = $default_row;
        $build[$category->parents[0]]['#rows'][$tid]['category']['data']['#markup'] = $category->label();
        $stat_rows[] = &$build[$category->parents[0]]['#footer'][0];
        $stat_rows[] = &$build[$category->parents[0]]['#rows'][$tid];
      }

      if (isset($stats[$tid])) {
        foreach ($stat_rows as &$stat_row) {
          foreach ($stats[$tid] as $state => $count) {
            $stat_row[$state] += $count;
            $stat_row['total']['data']['#markup'] += $count;
          }
        }
      }
    }

    return $build;
  }

  /**
   * List teams a user leads.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user.
   *
   * @return array
   *   Render array of teams the user leads.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function myTeams(User $user) {
    $storage = $this->entityTypeManager()
      ->getStorage('c_events_team');
    $query = $storage->getQuery();
    $query->accessCheck();
    $query->condition('leaders', $user->id());
    $query->sort('event.entity.date__value', 'DESC');
    $query->sort('event.entity.title', 'DESC');
    $query->sort('name', 'ASC');
    $query->pager(25);
    $teams = $storage->loadMultiple($query->execute());

    $build['table'] = [
      '#type' => 'table',
      '#header' => [
        'event' => $this->t('Event'),
        'team' => $this->t('Team'),
      ],
      '#rows' => [],
      '#empty' => $this->t("You aren't the leader of any teams."),
    ];

    foreach ($teams as $team) {
      $url = Url::fromRoute('entity.c_events_team.canonical', [
        'contacts_event' => $team->getEvent()->id(),
        'c_events_team' => $team->id(),
      ]);
      $link = Link::fromTextAndUrl($team->getName(), $url);

      $row = [
        'event' => $team->getEvent()->label(),
        'team' => $link,
      ];

      $build['table']['#rows'][$team->id()] = $row;
    }

    $build['pager'] = ['#type' => 'pager'];

    $build['#cache'] = ['max-age' => 0];

    return $build;
  }

}

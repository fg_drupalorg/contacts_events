<?php

namespace Drupal\contacts_events_teams;

use Drupal\contacts_events\Access\EventAccessTrait;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events_teams\Entity\TeamInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityHandlerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Access controller for the Team entity.
 *
 * @see \Drupal\contacts_events_teams\Entity\Team.
 */
class TeamAccessControlHandler extends EntityAccessControlHandler implements EntityHandlerInterface {

  use EventAccessTrait;

  /**
   * {@inheritdoc}
   */
  protected $viewLabelOperation = TRUE;

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return (new static($entity_type))
      ->setRouteMatch($container->get('current_route_match'))
      ->setEventStorage($container->get('entity_type.manager')->getStorage('contacts_event'));
  }

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\contacts_events_teams\Entity\TeamInterface $entity */
    $event = $entity->getEvent();

    // Globally forbid if teams are not enabled for the event.
    if (!$this->checkEvent($event)) {
      return AccessResult::forbidden()
        ->addCacheableDependency($event);
    }

    /** @var \Drupal\contacts_events_teams\Entity\Team $entity */
    switch ($operation) {
      // Everyone needs view label.
      case 'view label':
        return AccessResult::allowed();

      // View is about managing applications.
      case 'view':
        return AccessResult::allowedIfHasPermission($account, 'manage all contacts events team applications')
          ->orIf(AccessResult::allowedIf($entity->isTeamLeader($account->id())))
          ->setCacheMaxAge(0);

      // All other permissions are tied to the manage permission.
      case 'update':
      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'manage contacts events teams');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    // If we can find an event, globally forbid if not enabled for the event.
    if ($event = $this->getEventFromContextOrRoute($context)) {
      if (!$this->checkEvent($event)) {
        return AccessResult::forbidden()
          ->addCacheableDependency($event);
      }
    }

    $access = parent::checkCreateAccess($account, $context, $entity_bundle);

    // Only modify if it's worth doing.
    if ($access->isNeutral()) {
      $access = $access->orIf(AccessResult::allowedIfHasPermission($account, 'manage contacts events teams'));
    }

    return $access;
  }

  /**
   * Check whether teams are enabled for the event.
   *
   * @param \Drupal\contacts_events\Entity\EventInterface $event
   *   The event the team is for.
   *
   * @return bool
   *   Whether teams are enabled.
   */
  protected function checkEvent(EventInterface $event) {
    return $event->getSetting('teams.enabled', TeamInterface::STATUS_CLOSED) !== TeamInterface::STATUS_CLOSED;
  }

}

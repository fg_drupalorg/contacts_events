<?php

namespace Drupal\Tests\contacts_events\Unit;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\contacts_events\BookingSupervisionHelper;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\contacts_events\SupervisionHelper;
use Drupal\Core\Field\EntityReferenceFieldItemListInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Tests\UnitTestCase;

/**
 * Test the supervision helper.
 *
 * @group contacts_events
 *
 * @coversDefaultClass \Drupal\contacts_events_accommodation\BookingAccommodationHelper
 */
class BookingSupervisionHelperTest extends UnitTestCase {

  /**
   * Test the calculation of delegates on a booking.
   *
   * @param \Drupal\Component\Datetime\DateTimePlus $event_date
   *   The event start timestamp.
   * @param array $items
   *   An array of order item data. Each item is an array containing:
   *   - bundle: The bundle of the order item.
   *   - date_of_birth: The date to check delegate age against.
   * @param int $adult_total
   *   The expected confirmed count.
   * @param int $child_total
   *   The expected total count.
   *
   * @dataProvider dataDelegateCounts
   */
  public function testDelegateCounts(DateTimePlus $event_date, array $items, int $adult_total, int $child_total) {
    $order_items = [];

    $event = $this->prophesize(EventInterface::class);
    $event->get('date')
      ->willReturn((object) ['start_date' => $event_date]);

    foreach ($items as $item) {
      $ticket = $this->prophesize(TicketInterface::class);

      // @todo Add tests for cancelled tickets being ignored.
      $ticket->getStatus()
        ->willReturn('confirmed');

      $ticket->hasField('date_of_birth')
        ->willReturn(TRUE);
      $ticket->get('date_of_birth')
        ->willReturn((object) ['date' => new DateTimePlus($item['date_of_birth'])]);

      $ticket->getEvent()
        ->willReturn($event->reveal());

      $order_item = $this->prophesize(OrderItemInterface::class);
      $order_item->bundle()
        ->shouldBeCalledTimes(1)
        ->willReturn($item['bundle']);
      $order_item->getPurchasedEntity()
        ->willReturn($ticket->reveal());
      $order_items[] = $order_item->reveal();
    }

    $item_list = $this->prophesize(EntityReferenceFieldItemListInterface::class);
    $item_list->referencedEntities()
      ->shouldBeCalledTimes(1)
      ->willReturn($order_items);

    $helper = new BookingSupervisionHelper($item_list->reveal());
    $this->assertSame($adult_total, $helper->getAdultDelegates(), 'Adult delegates');
    $this->assertSame($child_total, $helper->getNonAdultDelegates(), 'Non adult delegates');
  }

  /**
   * Data provider for ::testDelegateCounts.
   */
  public function dataDelegateCounts() {
    yield 'no-data' => [
      (new DateTimePlus('2019-06-01')),
      [],
      0,
      0,
    ];

    yield 'only-one-child-ticket' => [
      (new DateTimePlus('2019-06-01')),
      [
        ['bundle' => 'contacts_ticket', 'date_of_birth' => '2017-01-01'],
      ],
      0,
      1,
    ];

    yield 'only-one-adult-ticket' => [
      (new DateTimePlus('2019-06-01')),
      [
        ['bundle' => 'contacts_ticket', 'date_of_birth' => '1980-01-01'],
      ],
      1,
      0,
    ];

    yield 'mixed-tickets' => [
      (new DateTimePlus('2019-06-01')),
      [
        ['bundle' => 'contacts_ticket', 'date_of_birth' => '1980-01-01'],
        ['bundle' => 'contacts_ticket', 'date_of_birth' => '2017-01-01'],
        ['bundle' => 'contacts_ticket', 'date_of_birth' => '2017-01-01'],
      ],
      1,
      2,
    ];
  }

  /**
   * Test the validation of delegate supervision on a booking.
   *
   * @param int $adult_total
   *   The adult count.
   * @param int $child_total
   *   The non adult count.
   * @param int $ratio
   *   The supervision ratio.
   * @param int $result
   *   The expected validation result.
   *
   * @dataProvider dataDelegateValidation
   */
  public function testDelegateValidation(int $adult_total, int $child_total, int $ratio, int $result) {
    $messenger = $this->prophesize(MessengerInterface::class);
    $helper = new SupervisionHelper($messenger->reveal());
    $return = $helper->validateSupervision($adult_total, $child_total, $ratio);
    $this->assertSame($result, $return);
  }

  /**
   * Data provider for ::testDelegateValidation.
   */
  public function dataDelegateValidation() {
    yield 'no-data' => [
      0,
      0,
      1,
      SupervisionHelper::VALIDATE_UNNECESSARY,
    ];

    yield 'only-one-child-ticket' => [
      0,
      1,
      1,
      SupervisionHelper::VALIDATE_NONE,

    ];

    yield 'only-one-adult-ticket' => [
      1,
      0,
      1,
      SupervisionHelper::VALIDATE_UNNECESSARY,

    ];

    yield 'not-enough-tickets' => [
      1,
      2,
      1,
      SupervisionHelper::VALIDATE_TOO_FEW,
    ];

    yield 'enough-tickets' => [
      1,
      1,
      1,
      SupervisionHelper::VALIDATE_OK,
    ];

    yield 'complex-not-enough-tickets' => [
      2,
      9,
      4,
      SupervisionHelper::VALIDATE_TOO_FEW,
    ];

    yield 'complex-enough-tickets' => [
      2,
      8,
      4,
      SupervisionHelper::VALIDATE_OK,
    ];
  }

}

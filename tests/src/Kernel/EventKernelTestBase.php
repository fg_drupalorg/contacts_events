<?php

namespace Drupal\Tests\contacts_events\Kernel;

use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\commerce_store\Entity\Store;
use Drupal\contacts_events\Entity\Event;
use Drupal\Tests\commerce\Kernel\CommerceKernelTestBase;

/**
 * Provides a base class for Contacts events kernel tests.
 */
abstract class EventKernelTestBase extends CommerceKernelTestBase {

  use ReusableSetupTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'commerce',
    'commerce_checkout',
    'commerce_order',
    'commerce_price',
    'commerce_payment',
    'commerce_store',
    'contacts',
    'contacts_events',
    'ctools',
    'datetime',
    'datetime_range',
    'entity',
    'entity_reference_revisions',
    'facets',
    'field',
    'file',
    'image',
    'search_api',
    'state_machine',
    'profile',
    'name',
    'image',
    'views',
    'views_data_export',
    'rest',
    'serialization',
  ];

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * The default store.
   *
   * @var \Drupal\commerce_payment\Entity\PaymentGateway
   */
  protected $gateway;

  /**
   * The default event.
   *
   * @var \Drupal\contacts_events\Entity\Event
   */
  protected $event;

  /**
   * {@inheritdoc}
   */
  protected function uncachedSetup() {
    $this->store = Store::load(1);
    $this->gateway = PaymentGateway::load('default');
    $this->event = Event::load($this->getEventData()['id']);
  }

  /**
   * {@inheritdoc}
   */
  protected function cachedSetup() {
    $this->installEntitySchema('file');
    $this->installEntitySchema('profile');
    $this->installEntitySchema('commerce_order');
    $this->installEntitySchema('commerce_order_item');
    $this->installEntitySchema('commerce_payment');
    $this->installEntitySchema('contacts_event');
    $this->installConfig('contacts');
    $this->installConfig('commerce_order');
    $this->installConfig('commerce_payment');
    $this->installConfig('contacts_events');

    // Create our payment gateway.
    $this->gateway = PaymentGateway::create([
      'id' => 'default',
      'label' => 'Default',
      'plugin' => 'manual',
    ]);
    $this->gateway->save();

    // Create our event.
    $this->event = Event::create($this->getEventData());
    $this->event->save();
  }

  /**
   * Get data array for creating the event for this test.
   *
   * @return array
   *   Event entity data array.
   */
  protected function getEventData() {
    return [
      'type' => 'default',
      'id' => 101,
      'date' => [
        'value' => strtotime('now'),
        'end_value' => strtotime('now'),
      ],
      'title' => 'Other Event 101',
    ];
  }

}

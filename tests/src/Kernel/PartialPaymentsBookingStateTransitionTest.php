<?php

namespace Drupal\Tests\contacts_events\Kernel;

/**
 * Test the booking process transition events with partial payments.
 *
 * @group contacts_events
 *
 * @requires module commerce_partial_payments
 */
class PartialPaymentsBookingStateTransitionTest extends BookingStateTransitionTest {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['commerce_partial_payments'];

  /**
   * {@inheritdoc}
   */
  public function dataOnOrderPlaced() {
    $data = parent::dataOnOrderPlaced();

    $data['pending_not_paid_item_partial'] = [
      'ticket_state_before' => 'pending',
      'order_status' => TRUE,
      'ticket_state_after' => 'confirmed',
      'transition' => 'confirm',
      'additional' => [],
      'payment_values' => [
        'amount' => [
          'number' => '9',
          'currency_code' => 'USD',
        ],
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['pending_not_paid_item_full'] = [
      'ticket_state_before' => 'pending',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'confirmed_paid_in_full',
      'additional' => ['confirm', 'paid_in_full'],
      'payment_values' => [
        'amount' => [
          'number' => '9',
          'currency_code' => 'USD',
        ],
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '10',
            'currency_code' => 'USD',
          ],
          [
            'target_id' => 2,
            'number' => '-1',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['pending_paid_item_partial'] = [
      'ticket_state_before' => 'pending',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'confirmed_paid_in_full',
      'additional' => ['confirm', 'paid_in_full'],
      'payment_values' => [
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
          [
            'target_id' => 2,
            'number' => '1',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['pending_paid_item_full'] = [
      'ticket_state_before' => 'pending',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'confirmed_paid_in_full',
      'additional' => ['confirm', 'paid_in_full'],
      'payment_values' => [
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '10',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['confirmed_not_paid_item_partial'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => TRUE,
      'ticket_state_after' => 'confirmed',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'amount' => [
          'number' => '9',
          'currency_code' => 'USD',
        ],
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['confirmed_not_paid_item_full'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => TRUE,
      'ticket_state_after' => 'confirmed',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'amount' => [
          'number' => '9',
          'currency_code' => 'USD',
        ],
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['confirmed_paid_item_partial'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'paid_in_full',
      'additional' => [],
      'payment_values' => [
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
          [
            'target_id' => 2,
            'number' => '1',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['confirmed_paid_item_full'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'paid_in_full',
      'additional' => [],
      'payment_values' => [
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '10',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['paid_in_full_not_paid_item_partial'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => TRUE,
      'ticket_state_after' => 'confirmed',
      'transition' => 'payment_undone',
      'additional' => [],
      'payment_values' => [
        'amount' => [
          'number' => '9',
          'currency_code' => 'USD',
        ],
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['paid_in_full_not_paid_item_full'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'amount' => [
          'number' => '9',
          'currency_code' => 'USD',
        ],
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '10',
            'currency_code' => 'USD',
          ],
          [
            'target_id' => 2,
            'number' => '-1',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['paid_in_full_paid_item_partial'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
          [
            'target_id' => 2,
            'number' => '1',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['paid_in_full_paid_item_full'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => TRUE,
      'ticket_state_after' => 'paid_in_full',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '10',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['cancelled_not_paid_item_partial'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => TRUE,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'amount' => [
          'number' => '9',
          'currency_code' => 'USD',
        ],
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['cancelled_not_paid_item_full'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => TRUE,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'amount' => [
          'number' => '9',
          'currency_code' => 'USD',
        ],
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '10',
            'currency_code' => 'USD',
          ],
          [
            'target_id' => 2,
            'number' => '-1',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['cancelled_paid_item_partial'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => TRUE,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '9',
            'currency_code' => 'USD',
          ],
          [
            'target_id' => 2,
            'number' => '1',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    $data['cancelled_paid_item_full'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => TRUE,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
      'payment_values' => [
        'order_item_tracking' => [
          [
            'target_id' => 1,
            'number' => '10',
            'currency_code' => 'USD',
          ],
        ],
      ],
    ];

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function dataOnPaymentMade() {
    $data = parent::dataOnPaymentMade();

    $data['pending_not_paid_item_partial'] = [
      'ticket_state_before' => 'pending',
      'order_status' => NULL,
      'ticket_state_after' => 'pending',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '9',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['pending_not_paid_item_full'] = [
      'ticket_state_before' => 'pending',
      'order_status' => NULL,
      'ticket_state_after' => 'pending',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '10',
          'currency_code' => 'USD',
        ],
        [
          'target_id' => 2,
          'number' => '-1',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['pending_paid_item_partial'] = [
      'ticket_state_before' => 'pending',
      'order_status' => NULL,
      'ticket_state_after' => 'pending',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '9',
          'currency_code' => 'USD',
        ],
        [
          'target_id' => 2,
          'number' => '1',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['pending_paid_item_full'] = [
      'ticket_state_before' => 'pending',
      'order_status' => NULL,
      'ticket_state_after' => 'pending',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '10',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['confirmed_not_paid_item_partial'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => NULL,
      'ticket_state_after' => 'confirmed',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '9',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['confirmed_not_paid_item_full'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => NULL,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'paid_in_full',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '10',
          'currency_code' => 'USD',
        ],
        [
          'target_id' => 2,
          'number' => '-1',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['confirmed_paid_item_partial'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => NULL,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'paid_in_full',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '9',
          'currency_code' => 'USD',
        ],
        [
          'target_id' => 2,
          'number' => '1',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['confirmed_paid_item_full'] = [
      'ticket_state_before' => 'confirmed',
      'order_status' => NULL,
      'ticket_state_after' => 'paid_in_full',
      'transition' => 'paid_in_full',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '10',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['paid_in_full_not_paid_item_partial'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => NULL,
      'ticket_state_after' => 'confirmed',
      'transition' => 'payment_undone',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '9',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['paid_in_full_not_paid_item_full'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => NULL,
      'ticket_state_after' => 'paid_in_full',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '10',
          'currency_code' => 'USD',
        ],
        [
          'target_id' => 2,
          'number' => '-1',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['paid_in_full_paid_item_partial'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => NULL,
      'ticket_state_after' => 'paid_in_full',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '9',
          'currency_code' => 'USD',
        ],
        [
          'target_id' => 2,
          'number' => '1',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['paid_in_full_paid_item_full'] = [
      'ticket_state_before' => 'paid_in_full',
      'order_status' => NULL,
      'ticket_state_after' => 'paid_in_full',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '10',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['cancelled_not_paid_item_partial'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => NULL,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '9',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['cancelled_not_paid_item_full'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => NULL,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '10',
          'currency_code' => 'USD',
        ],
        [
          'target_id' => 2,
          'number' => '-1',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['cancelled_paid_item_partial'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => NULL,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '9',
          'currency_code' => 'USD',
        ],
        [
          'target_id' => 2,
          'number' => '1',
          'currency_code' => 'USD',
        ],
      ],
    ];

    $data['cancelled_paid_item_full'] = [
      'ticket_state_before' => 'cancelled',
      'order_status' => NULL,
      'ticket_state_after' => 'cancelled',
      'transition' => '',
      'additional' => [],
      'tracking' => [
        [
          'target_id' => 1,
          'number' => '10',
          'currency_code' => 'USD',
        ],
      ],
    ];

    return $data;
  }

}

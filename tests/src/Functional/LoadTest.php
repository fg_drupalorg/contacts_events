<?php

namespace Drupal\Tests\contacts_events\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group contacts_events
 */
class LoadTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   *
   * @todo Remove bookkeeping once
   * https://www.drupal.org/project/drupal/issues/3044920 is resolved.
   */
  protected static $modules = [
    'toolbar',
    'bookkeeping',
    'contacts_events',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Tests that the home page loads with a 200 response.
   */
  public function testLoad() {
    $user = $this->drupalCreateUser();
    $this->drupalLogin($user);
    $this->drupalGet(Url::fromRoute('<front>'));
    $this->assertSession()->statusCodeEquals(200);
  }

}

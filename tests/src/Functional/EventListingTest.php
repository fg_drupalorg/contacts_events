<?php

namespace Drupal\Tests\contacts_events\Functional;

use Drupal\contacts_events\Entity\Event;
use Drupal\contacts_events\Entity\EventInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Tests\BrowserTestBase;

/**
 * Simple test to ensure that main page loads with module enabled.
 *
 * @group contacts_events
 */
class EventListingTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   *
   * @todo Remove bookkeeping once
   * https://www.drupal.org/project/drupal/issues/3044920 is resolved.
   */
  protected static $modules = [
    'toolbar',
    'bookkeeping',
    'contacts_events',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * Tests that the event listing page shows the correct events.
   */
  public function testEventList() {
    $user = $this->drupalCreateUser([
      'view published contacts_event entities',
      'can book for contacts_events',
    ]);
    $this->drupalLogin($user);

    // Before any events are added, there should be some empty text.
    $this->drupalGet('events');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('There are no upcoming events.');

    // Add a selection of events.
    $listed_events = [];

    // No date, shouldn't show on the listing.
    $this->addEvent(EventInterface::STATUS_DISABLED);

    $now = (new DrupalDateTime())->setTime(10, 0, 0);

    // Past event, shouldn't show on the listing.
    $date = [
      'value' => (clone $now)->sub(new \DateInterval('P10D'))->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => (clone $now)->sub(new \DateInterval('P10D'))->setTime(17, 0)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
    ];
    $this->addEvent(EventInterface::STATUS_OPEN, $date);

    // Not published, shouldn't show on the listing.
    $date = [
      'value' => (clone $now)->add(new \DateInterval('P10D'))->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => (clone $now)->add(new \DateInterval('P10D'))->setTime(17, 0)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
    ];
    $this->addEvent(EventInterface::STATUS_OPEN, $date, FALSE);

    // Future, should show on the listing, not bookable.
    $date = [
      'value' => (clone $now)->add(new \DateInterval('P15D'))->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => (clone $now)->add(new \DateInterval('P15D'))->setTime(17, 0)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
    ];
    $event = $this->addEvent(EventInterface::STATUS_DISABLED, $date);
    $listed_events[] = [
      'id' => $event->id(),
      'title' => $event->label(),
      'date' => $date,
      'bookable' => FALSE,
    ];

    // Future, should show on the listing, not bookable.
    $date = [
      'value' => (clone $now)->add(new \DateInterval('P20D'))->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => (clone $now)->add(new \DateInterval('P20D'))->setTime(17, 0)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
    ];
    $event = $this->addEvent(EventInterface::STATUS_CLOSED, $date);
    $listed_events[] = [
      'id' => $event->id(),
      'title' => $event->label(),
      'date' => $date,
      'bookable' => FALSE,
    ];

    // Future, should show on the listing, bookable.
    $date = [
      'value' => (clone $now)->add(new \DateInterval('P25D'))->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
      'end_value' => (clone $now)->add(new \DateInterval('P25D'))->setTime(17, 0)->format(DateTimeItemInterface::DATETIME_STORAGE_FORMAT),
    ];
    $event = $this->addEvent(EventInterface::STATUS_OPEN, $date);
    $listed_events[] = [
      'id' => $event->id(),
      'title' => $event->label(),
      'date' => $date,
      'bookable' => TRUE,
    ];

    $this->drupalGet('events');
    $this->assertSession()->statusCodeEquals(200);

    // Ensure each of the expected events is there in the expected format.
    $page = $this->getSession()->getPage();
    $views_rows = $page->findAll('css', '.view-contacts-events-listings > .view-content > .views-row');

    $this->assertSameSize($listed_events, $views_rows, 'Expected number of events');

    foreach ($views_rows as $delta => $row) {
      $expected_event = $listed_events[$delta];

      // Check the event title is showing correctly.
      $this->assertStringContainsString($expected_event['title'], $row->getText(), "Event {$delta} title missing");

      // Check the more info link is showing correctly.
      $more_link = $row->findLink('More info');
      if ($more_link) {
        /** @var \Behat\Mink\Element\NodeElement $more_link */
        $this->assertEquals("/event/{$expected_event['id']}", $more_link->getAttribute('href'), "Event {$delta} more info link has wrong URL");
      }
      else {
        $this->fail("Event {$delta} more info link missing");
      }

      // Ensure the book now link is showing correctly when it should be
      // available and doesn't exist when it shouldn't.
      $book_link = $row->findLink('Book now');
      if ($book_link) {
        /** @var \Behat\Mink\Element\NodeElement $book_link */
        if ($expected_event['bookable']) {
          $this->assertEquals("/event/{$expected_event['id']}/book", $book_link->getAttribute('href'), "Event {$delta} book now link has wrong URL");
        }
        else {
          $this->fail("Event {$delta} more info link missing");
        }
      }
      elseif ($expected_event['bookable']) {
        $this->fail("Event {$delta} book now link missing");
      }
    }
  }

  /**
   * Add an event.
   *
   * @param string $booking_status
   *   The booking status. One of the EventInterface::STATUS_* constants.
   * @param array|null $date
   *   The date field values, or NULL for none.
   * @param bool $published
   *   Whether the event is published.
   *
   * @return \Drupal\contacts_events\Entity\EventInterface
   *   The created and saved event.
   */
  protected function addEvent(string $booking_status, ?array $date = NULL, bool $published = TRUE) {
    $event = Event::create([
      'type' => 'default',
      'title' => $this->randomString(),
      'code' => $this->randomMachineName(4),
      'status' => $published,
      'booking_status' => $booking_status,
      'date' => $date,
    ]);
    $event->save();
    return $event;
  }

}

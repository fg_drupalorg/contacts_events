<?php

namespace Drupal\contacts_events\EventSubscriber;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Event\OrderEvent;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_stripe\EventSubscriber\OrderPaymentIntentSubscriber;
use Drupal\commerce_stripe\Plugin\Commerce\PaymentGateway\StripeInterface;
use Drupal\Core\TempStore\PrivateTempStoreFactory;

/**
 * Contacts Events event subscriber.
 */
class StripeIntentOrderSubscriber extends OrderPaymentIntentSubscriber {

  /**
   * The contacts events booking flow private tempstore.
   *
   * @var \Drupal\Core\TempStore\PrivateTempStore
   */
  private $tempstore;

  /**
   * Set the private tempstore.
   *
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $factory
   *   The private temp store factory.
   *
   * @return $this
   */
  public function setPrivateTempstore(PrivateTempStoreFactory $factory) {
    $this->tempstore = $factory->get('contacts_events.booking_flow');
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function onOrderUpdate(OrderEvent $event) {
    $order = $event->getOrder();
    $this->updateOrder($order);
  }

  /**
   * Mark an order's intent for update, if there is one.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order to check and, if required, mark for update.
   */
  public function updateOrder(OrderInterface $order): void {
    $gateway = $order->get('payment_gateway');
    if ($gateway->isEmpty() || !$gateway->entity instanceof PaymentGatewayInterface) {
      return;
    }
    $plugin = $gateway->entity->getPlugin();
    if (!$plugin instanceof StripeInterface) {
      return;
    }
    $intent_id = $order->getData('stripe_intent');
    if ($intent_id === NULL) {
      return;
    }

    $total_price = $this->getPaymentAmount($order);
    if ($total_price !== NULL) {
      $amount = $this->minorUnitsConverter->toMinorUnits($total_price);
      $this->updateList[$intent_id] = $amount;
    }
  }

  /**
   * Get the payment amount from the private tempstore.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order.
   *
   * @return \Drupal\commerce_price\Price|null
   *   The payment amount, or NULL if there is none specified.
   */
  private function getPaymentAmount(OrderInterface $order): ?Price {
    $amount = $this->tempstore->get("{$order->id()}:payment_amount");
    return $amount ? Price::fromArray($amount) : $order->getBalance();
  }

}

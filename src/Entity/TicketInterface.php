<?php

namespace Drupal\contacts_events\Entity;

use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface for defining Ticket entities.
 *
 * @ingroup contacts_events
 */
interface TicketInterface extends SingleUsePurchasableEntityInterface, EntityChangedInterface {

  /**
   * Get the booking the ticket belongs to.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface|null
   *   The order this ticket belongs to.
   */
  public function getBooking();

  /**
   * Get the event the ticket is for.
   *
   * @return \Drupal\contacts_events\Entity\EventInterface|null
   *   The event this ticket is for.
   */
  public function getEvent();

  /**
   * Get the formatted name.
   *
   * @param string $type
   *   Selects the text processing to be applied to the name.
   *
   * @see _name_value_sanitize()
   */
  public function getName($type = 'default');

  /**
   * Gets the Ticket creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Ticket.
   */
  public function getCreatedTime();

  /**
   * Sets the Ticket creation timestamp.
   *
   * @param int $timestamp
   *   The Ticket creation timestamp.
   *
   * @return \Drupal\contacts_events\Entity\TicketInterface
   *   The called Ticket entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Performs acquisitions on this ticket using the email address.
   *
   * @param bool $early
   *   Whether this is an early acquisition. For early acquisitions, we don't
   *   create a user, save anything or update profiles.
   */
  public function acquire($early = FALSE);

  /**
   * Current ticket status.
   *
   * @return string
   *   The ticket status.
   */
  public function getStatus();

  /**
   * Gets the timestamp of when the ticket was confirmed.
   *
   * @return int|null
   *   Confirmed timestamp or NULL if not confirmed.
   */
  public function getConfirmedDate();

  /**
   * Gets the ticketholder.
   *
   * @return \Drupal\user\UserInterface
   *   The user that represents the ticketholder.
   */
  public function getTicketHolder() : ?UserInterface;

  /**
   * Gets the ticketholder ID.
   *
   * @return int
   *   The user ID represents the ticketholder.
   */
  public function getTicketHolderId() : ?int;

  /**
   * Sets the Ticketholder.
   *
   * @param int $uid
   *   The ID of the user.
   *
   * @return $this
   */
  public function setTicketHolderId($uid);

  /**
   * Sets the ticketholder.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user that represents the ticketholder.
   *
   * @return $this
   */
  public function setTicketHolder(UserInterface $account);

}

<?php

namespace Drupal\contacts_events\Entity;

use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Class entities.
 */
interface EventClassInterface extends ConfigEntityInterface {

  /**
   * Evaluate the condition.
   *
   * @param \Drupal\commerce_order\Entity\OrderItemInterface $order_item
   *   An array of context values keyed by context name.
   *
   * @return bool
   *   Whether the class conditions match the given context.
   */
  public function evaluate(OrderItemInterface $order_item);

}

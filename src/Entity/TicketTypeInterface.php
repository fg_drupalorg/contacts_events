<?php

namespace Drupal\contacts_events\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Ticket type entities.
 */
interface TicketTypeInterface extends ConfigEntityInterface {

}

<?php

namespace Drupal\contacts_events\Form;

use Drupal\contacts_events\Entity\TicketInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Trait for triggering ticket form hooks.
 */
trait TicketFormHookTrait {

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Trigger the ticket form alter hook.
   *
   * We manually trigger as we have too many arguments for
   * ModuleHandlerInterface::alter.
   *
   * @param array $form
   *   The ticket form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param \Drupal\contacts_events\Entity\TicketInterface $ticket
   *   The ticket.
   * @param string $display_mode
   *   The display mode.
   */
  protected function alter(array &$form, FormStateInterface $form_state, TicketInterface $ticket, string $display_mode) {
    $this->moduleHandler->invokeAllWith('contacts_events_ticket_form_alter', function (callable $hook, string $module) use (&$form, $form_state, $ticket, $display_mode) {
      $hook($form, $form_state, $ticket, $display_mode);
    });
  }

}

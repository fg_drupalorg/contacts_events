<?php

namespace Drupal\contacts_events\Form;

use Drupal\commerce_order\Form\OrderAddForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Extends OrderAddForm to add event field when creating Booking type orders.
 *
 * @todo Move to a form alter when Commerce merges order data issue
 * https://www.drupal.org/project/commerce/issues/3031005
 */
class BookingAddForm extends OrderAddForm {
  /**
   * Event entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $eventStorage;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($entity_type_manager);

    $this->eventStorage = $entity_type_manager->getStorage('contacts_event');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    // Replace the user search with the search API index.
    $form['customer']['uid'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Search'),
      '#attributes' => [
        'class' => ['container-inline'],
      ],
      '#placeholder' => $this->t('Search by username or email address'),
      '#target_type' => 'user',
      '#required' => TRUE,
      '#selection_handler' => 'search_api',
      '#selection_settings' => [
        'index' => 'contacts_index',
        'conditions' => [
          [
            'roles', 'crm_indiv', 'IN',
          ],
        ],
      ],
    ];

    $form['event'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'contacts_event',
      '#title' => $this->t('Event'),
      '#description' => $this->t('The event being booked.'),
      '#tags' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="type[value]"]' => ['value' => 'contacts_booking'],
        ],
        'required' => [
          ':input[name="type[value]"]' => ['value' => 'contacts_booking'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $is_booking = $form_state->getValue('type') == 'contacts_booking';
    $event_value = $form_state->getValue('event');
    $event_not_set = empty($event_value) || empty($this->eventStorage->load($event_value[0]['target_id']));

    if ($is_booking && $event_not_set) {
      $form_state->setError($form['event'], $this->t('A valid Event must be selected for Booking orders.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->submitCustomerForm($form, $form_state);

    $values = $form_state->getValues();
    $order_data = [
      'type' => $values['type'],
      'mail' => $values['mail'],
      'uid' => [$values['uid']],
      'store_id' => [$values['store_id']],
      'event' => $values['event'],
    ];

    // Set the placed date to the specified time or fall back to request time.
    if (!empty($values['custom_placed_date']) && !empty($values['placed'])) {
      $order_data['placed'] = $values['placed']->getTimestamp();
    }
    else {
      $order_data['placed'] = \Drupal::time()->getRequestTime();
    }
    $order = $this->orderStorage->create($order_data);

    // If this is a Booking order and an event is set, set the checkout step to
    // tickets.
    if ($values['type'] == 'contacts_booking') {
      $order->set('checkout_step', 'tickets');
    }

    $order->save();

    // Redirect to the edit form to complete the order.
    $form_state->setRedirect('entity.commerce_order.edit_form', ['commerce_order' => $order->id()]);
  }

}

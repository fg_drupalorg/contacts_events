<?php

namespace Drupal\contacts_events;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\PluralTranslatableMarkup;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Service for checking booking supervision ratios.
 */
class SupervisionHelper {

  /**
   * Validation is not needed.
   */
  const VALIDATE_UNNECESSARY = -1;

  /**
   * Validation has passed.
   */
  const VALIDATE_OK = 0;

  /**
   * Validation failed: No adults.
   */
  const VALIDATE_NONE = 1;

  /**
   * Validation failed: Too few adults.
   */
  const VALIDATE_TOO_FEW = 2;

  /**
   * The messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Construct the ExpandedSupervisionHelper.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(MessengerInterface $messenger) {
    $this->messenger = $messenger;
  }

  /**
   * Get the booking helper service for this booking.
   *
   * @param \Drupal\Core\Field\EntityReferenceFieldItemList $items
   *   The order items to be reviewed.
   *
   * @return \Drupal\contacts_events\BookingSupervisionHelperInterface
   *   The booking helper service.
   */
  public function getBookingHelper(EntityReferenceFieldItemList $items) : BookingSupervisionHelperInterface {
    return new BookingSupervisionHelper($items);
  }

  /**
   * Get the supervision ratio for this event.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The booking being checked.
   *
   * @return int|null
   *   The ratio to be checked or NULL if not configured.
   */
  public function getRatio(OrderInterface $order) {
    /** @var \Drupal\contacts_events\Entity\EventInterface $event */
    $event = $order->get('event')->entity;

    if ($event->hasField('supervision_ratio')) {
      return $event->get('supervision_ratio')->value;
    }
  }

  /**
   * Check booking supervision and show status message.
   *
   * If there is a problem, a warning message will be shown, status otherwise.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The booking to be checked.
   * @param bool $error_only
   *   If status message should only be shown for errors.
   */
  public function showMessage(OrderInterface $order, $error_only = FALSE) {
    $allowed_ratio = $this->getRatio($order);
    if ($allowed_ratio) {
      // Get the booking helper to calculate current ratios.
      $booking_helper = $this->getBookingHelper($order->get('order_items'));
      $adults_total = $booking_helper->getAdultDelegates();
      $non_adult_total = $booking_helper->getNonAdultDelegates();
      $ratio_result = $this->validateSupervision($adults_total, $non_adult_total, $allowed_ratio);

      if ($ratio_result <= static::VALIDATE_OK && $error_only) {
        return;
      }

      // Build the base text for the alert.
      $alert_text = new TranslatableMarkup('@title requires an adult for every @allowed_ratio.', [
        '@title' => $order->get('event')->entity->label(),
        '@allowed_ratio' => new PluralTranslatableMarkup($allowed_ratio, 'under-18', '@count under-18s'),
      ]);
      $required_text = '';
      $alert = 'status';

      // If there are additional requirements add them to the alert text.
      if ($ratio_result > static::VALIDATE_OK) {
        $required_adults = ceil($non_adult_total / $allowed_ratio) - $adults_total;
        $required_text = ' ' . new TranslatableMarkup('so you need @adults_required', [
          '@adults_required' => new PluralTranslatableMarkup($required_adults, 'one more adult', '@count more adults'),
        ]);
        $alert = 'warning';
      }

      $alert_text = new TranslatableMarkup('@ratios Your booking currently has @children and @adults@required.', [
        '@ratios' => $alert_text,
        '@children' => new PluralTranslatableMarkup($non_adult_total, 'one under-18', '@count under-18s'),
        '@adults' => new PluralTranslatableMarkup($adults_total, 'one adult', '@count adults'),
        '@required' => $required_text,
      ]);
      $this->messenger->addMessage($alert_text, $alert);
    }
  }

  /**
   * Validate that the booking meets the required ratio.
   *
   * @param int $adult_total
   *   Total number of adult delegates.
   * @param int $non_adult_total
   *   Total number of non adult delegates.
   * @param int $ratio
   *   Required adult to child ratio.
   *
   * @return int
   *   The validation result. Once of the self::VALIDATE_* constants.
   */
  public function validateSupervision(int $adult_total, int $non_adult_total, int $ratio) : int {
    if ($non_adult_total == 0) {
      return $this::VALIDATE_UNNECESSARY;
    }

    // Check there is at least one adult.
    if ($adult_total == 0) {
      return $this::VALIDATE_NONE;
    }

    // Check that the number of adults satisfies the supervision ratio.
    if (($adult_total / $non_adult_total) < (1 / $ratio)) {
      return $this::VALIDATE_TOO_FEW;
    }

    // All checks have passed.
    return $this::VALIDATE_OK;
  }

}

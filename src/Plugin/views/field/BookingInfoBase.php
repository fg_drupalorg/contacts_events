<?php

namespace Drupal\contacts_events\Plugin\views\field;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Base class for fields that retrieve a booking based on the selected user.
 *
 * @package Drupal\contacts_events\Plugin\views\field
 */
abstract class BookingInfoBase extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['argument'] = ['default' => ''];
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['argument'] = [
      '#type' => 'select',
      '#title' => $this->t('User argument'),
      '#default_value' => $this->options['argument'],
      '#options' => [],
      '#required' => TRUE,
    ];
    /** @var \Drupal\views\Plugin\views\ViewsHandlerInterface $handler */
    foreach ($this->view->getDisplay()->getHandlers('argument') as $id => $handler) {
      $form['argument']['#options'][$id] = $handler->adminLabel(TRUE);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validate() {
    $errors = parent::validate();

    if (!isset($this->options['argument'])) {
      $errors[] = $this->t('The user argument is required: @filter.', ['@filter' => $this->adminLabel(TRUE)]);
    }
    elseif (!isset($this->view->getHandlers('argument')[$this->options['argument']])) {
      $errors[] = $this->t('The specified user argument does not exist: @filter.', ['@filter' => $this->adminLabel(TRUE)]);
    }

    return $errors;
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $booking */
    $booking = $this->getEntity($values);
    if ($booking->bundle() != 'contacts_booking') {
      return NULL;
    }

    $argument = $this->view->getDisplay()->getHandler('argument', $this->options['argument']);
    $uid = $argument->getValue();
    if (!$uid) {
      return NULL;
    }

    return $this->renderBooking($booking, $uid);
  }

  /**
   * Generates the row output.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $booking
   *   Current order.
   * @param string $uid
   *   User ID.
   *
   * @return mixed
   *   Result to render.
   */
  abstract protected function renderBooking(OrderInterface $booking, $uid);

}

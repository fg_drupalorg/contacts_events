<?php

namespace Drupal\contacts_events\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;

/**
 * Order balance views field hander.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("contacts_events_booking_ticket_holders")
 */
class Ticketholders extends FieldPluginBase {

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $this->getEntity($values);

    $ticket_holders = [];

    foreach ($order->getItems() as $order_item) {
      // @todo We currently limit to confirmed/paid tickets. D7 provided a
      // configurable option to chose how this should be filtered. We might
      // want to add this option here too.
      // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
      if ($order_item->bundle() == 'contacts_ticket' && in_array($order_item->get('state')->value, ['confirmed', 'paid_in_full'])) {
        /** @var \Drupal\contacts_events\Entity\TicketInterface $ticket */
        $ticket = $order_item->getPurchasedEntity();
        if ($ticket_holder = $ticket->getTicketHolder()) {
          $ticket_holders[] = $ticket_holder->getDisplayName();
        }
        else {
          // If there is no contact, just pull the name from the ticket.
          $ticket_holders[] = $ticket->getName();
        }
      }
    }

    return implode(', ', $ticket_holders);
  }

}

<?php

namespace Drupal\contacts_events\Plugin\views\argument;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\views\Plugin\views\argument\ArgumentPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a contextual filter for Bookings for a User.
 *
 * @ingroup views_argument_handlers
 *
 * @ViewsArgument("contacts_events_user_bookings")
 */
class UserBookingsArgument extends ArgumentPluginBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs the UserBookingArgument object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query($group_by = FALSE) {
    // Find bookings this user owns.
    $owner_order_ids = $this->entityTypeManager
      ->getStorage('commerce_order')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $this->argument)
      ->execute();

    // Find bookings this user has tickets for.
    $delegate_order_ids = $this->entityTypeManager
      ->getStorage('contacts_ticket')
      ->getAggregateQuery()
      ->groupBy('order_item.entity.order_id')
      ->accessCheck(FALSE)
      ->condition('contact', $this->argument)
      ->execute();

    $order_ids = array_unique(array_merge(
      $owner_order_ids,
      array_map(fn ($row) => $row['order_id'], $delegate_order_ids),
    ));
    if (count($order_ids)) {
      $this->ensureMyTable();
      $this->query->addWhere(0, "$this->tableAlias.$this->realField", $order_ids, 'IN');
    }
    else {
      $this->query->addWhereExpression(0, "0 = 1");
    }
  }

}

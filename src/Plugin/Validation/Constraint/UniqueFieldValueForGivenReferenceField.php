<?php

namespace Drupal\contacts_events\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks if a field has a unique value for across all entities of a given type.
 *
 * @Constraint(
 *   id = "UniqueFieldValueForGivenReferenceField",
 *   label = @Translation("Unique field for given reference field constraint", context = "Validation"),
 * )
 */
class UniqueFieldValueForGivenReferenceField extends Constraint {

  /**
   * The field on the parent entity which defines the reference field.
   *
   * @var string
   */
  protected $groupingField;

  /**
   * The the error message to display if constraint not met.
   *
   * @var string
   */
  protected $message;

  /**
   * Get the error message to display if constraint not met.
   *
   * @return string
   *   The message to be displayed.
   */
  public function getMessage() {
    return $this->message;
  }

  /**
   * Gets the grouping field option.
   *
   * @return string
   *   The grouping field name.
   */
  public function getGroupingFieldOption() {
    return $this->groupingField;
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultOption() {
    return 'groupingField';
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredOptions() {
    return [
      'groupingField',
      'message',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validatedBy() {
    return '\Drupal\contacts_events\Plugin\Validation\Constraint\UniqueFieldValueForGivenReferenceFieldValidator';
  }

}

<?php

namespace Drupal\contacts_events\Plugin\Field;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldItemList;

/**
 * Item list for the settings item.
 */
class SettingsItemList extends FieldItemList {

  /**
   * Get a particular setting.
   *
   * @param string|array $key
   *   Either a string or array indicating the key. Strings can indicate depth
   *   with '.'.
   * @param mixed $default
   *   The default value if there is no setting.
   *
   * @return mixed
   *   The setting, or the default value.
   */
  public function getSettingValue($key, $default = NULL) {
    if ($this->isEmpty()) {
      return $default;
    }

    if (!is_array($key)) {
      $key = explode('.', $key);
    }

    $settings = $this->first()->value;
    $result = NestedArray::getValue($settings, $key, $key_exists);

    if ($key_exists) {
      return $result;
    }

    return $default;
  }

  /**
   * Set a particular setting.
   *
   * @param string|array $key
   *   Either a string or array indicating the key. Strings can indicate depth
   *   with '.'.
   * @param mixed $value
   *   The value to set.
   *
   * @return $this
   */
  public function setSettingValue($key, $value) {
    if (!is_array($key)) {
      $key = explode('.', $key);
    }

    $item = $this->first() ?? $this->createItem(0, []);
    $settings = $item->value ?? [];
    NestedArray::setValue($settings, $key, $value);
    $this->value = $settings;

    return $this;
  }

  /**
   * Unset a particular setting.
   *
   * @param string|array $key
   *   Either a string or array indicating the key. Strings can indicate depth
   *   with '.'.
   *
   * @return $this
   */
  public function unsetSettingValue($key) {
    if (!is_array($key)) {
      $key = explode('.', $key);
    }

    $item = $this->first() ?? $this->createItem(0, []);
    $settings = $item->value ?? [];
    NestedArray::unsetValue($settings, $key);
    $this->value = $settings;

    return $this;
  }

}

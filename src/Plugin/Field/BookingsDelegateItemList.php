<?php

namespace Drupal\contacts_events\Plugin\Field;

use Drupal\Core\Field\EntityReferenceFieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\user\UserInterface;

/**
 * Computed item list for the bookings the user has a ticket on.
 *
 * @package Drupal\contacts_events\Plugin\Field
 */
class BookingsDelegateItemList extends EntityReferenceFieldItemList {

  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $user = $this->getEntity();
    if (!$user instanceof UserInterface) {
      throw new \Exception('BookingsManagedItemList is only suitable for fields on the user entity.');
    }

    // Instantiate the list and do nothing for anonymous users.
    $this->list = [];
    if ($user->isAnonymous()) {
      return;
    }

    $ticket_ids = \Drupal::entityQuery('contacts_ticket')
      ->accessCheck()
      ->condition('contact', $user->id())
      ->execute();

    if (!$ticket_ids) {
      return;
    }

    $ids = \Drupal::entityQuery('commerce_order')
      ->accessCheck(FALSE)
      ->condition('type', 'contacts_booking')
      ->condition('order_items.entity.type', 'contacts_ticket')
      // phpcs:ignore Drupal.Arrays.Array.LongLineDeclaration
      ->condition('order_items.entity.state', ['cancelled', 'pending'], 'NOT IN')
      ->condition('order_items.entity.purchased_entity', $ticket_ids, 'IN')
      ->execute();

    $delta = 0;
    foreach ($ids as $id) {
      $this->list[$delta] = $this->createItem($delta, $id);
      $delta++;
    }
  }

}

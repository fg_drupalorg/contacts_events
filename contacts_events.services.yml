services:
  logger.channel.contacts_events:
    parent: logger.channel_base
    arguments: ['contacts_events']
  contacts_events.ticket.acquire:
    class: Drupal\contacts_events\EventSubscriber\CreateIndividualProfileOnTicketAcquisition
    tags:
    - { name: 'event_subscriber' }
  contacts_events.price_calculator:
    class: Drupal\contacts_events\PriceCalculator
    arguments: ['@entity_type.manager', '@logger.channel.contacts_events', '@event_dispatcher']
  contacts_events.cron.recalculate_booking_windows:
    class: Drupal\contacts_events\Cron\RecalculateOnBookingWindow
    arguments: ['@state', '@datetime.time', '@entity_type.manager', '@entity_field.manager', '@contacts_events.price_calculator']
    calls:
      - ['setLogger', ['@logger.channel.contacts_events']]
  contacts_events.supervision_helper:
    class: Drupal\contacts_events\SupervisionHelper
    arguments: ['@messenger']
  contacts_events.user_bookings:
    class: Drupal\contacts_events\UserBookingsHelper
    arguments: ['@entity_type.manager', '@current_user', '@cache.contacts_events_user_bookings']

  # Access checks.
  access_check.contacts_events.order_is_booking:
    class: Drupal\contacts_events\Access\BookingAccessChecker
    tags:
    - { name: access_check, applies_to: _contacts_events_is_booking }
  # Route subscribers
  contacts_events.order_add_route_subscriber:
    class: Drupal\contacts_events\Routing\OrderAddRouteSubscriber
    tags:
      - { name: event_subscriber }
  contacts_events.admin_theme_subscriber:
    class: Drupal\contacts_events\Routing\ViewsAdminRouteSubscriber
    tags:
      - { name: event_subscriber }

  # State machine
  contacts_events.booking_confirmed:
    class: Drupal\contacts_events\EventSubscriber\BookingConfirmedSubscriber
    arguments: ['@datetime.time', '@event_dispatcher']
    calls:
      - [setOrderItemTracking, ['@?commerce_partial_payments.order_item_tracking']]
    tags:
      - { name: event_subscriber }
  contacts_events.workflow_guard:
    class: Drupal\contacts_events\Guard\WorkflowGuard
    tags:
    - { name: state_machine.guard, group: commerce_order }

  # Payment events.
  contacts_events.subscriber.payment_data:
    class: Drupal\contacts_events\EventSubscriber\PaymentDataSubscriber
    tags:
    - { name: 'event_subscriber' }
  contacts_events.subscriber.payment_gateway_filter:
    class: Drupal\contacts_events\EventSubscriber\PaymentGatewayFilter
    arguments: ['@current_route_match']
    tags:
      - { name: event_subscriber }

  # Booking completion events.
  contacts_events.booking_completion_validation:
    class: Drupal\contacts_events\EventSubscriber\BookingCompletionValidationSubscriber
    arguments: ['@contacts_events.supervision_helper', '@entity_type.manager', '@config.factory']
    tags:
      - { name: event_subscriber }

  # Entity hooks.
  contacts_events.entity_hooks.commerce_order:
    class: Drupal\contacts_events\Entity\OrderHooks
    arguments: ['@current_user']
    calls:
      - [setOrderItemTracking, ['@?commerce_partial_payments.order_item_tracking']]
  contacts_events.entity_hooks.commerce_order_item:
    class: Drupal\contacts_events\Entity\OrderItemHooks
    arguments: ['@current_route_match']
    calls:
      - [setOrderItemTracking, ['@?commerce_partial_payments.order_item_tracking']]
  contacts_events.entity_hooks.commerce_payment:
    class: Drupal\contacts_events\Entity\PaymentHooks

  # User cancel confirm subscriber.
  contacts_events.user_cancel_confirm.ticket_subscriber:
    class: Drupal\contacts_events\EventSubscriber\TicketUserCancelConfirmationSubscriber
    arguments: ['@database']
    tags:
      - { name: event_subscriber }
  # Date range formatter
  contacts_events.date_range_compact.formatter:
    class: Drupal\contacts_events\DateRangeFormatter
    arguments: ['@date.formatter']

  # Form alters.
  contacts_events.form_alter.booking_admin_tickets:
    class: Drupal\contacts_events\Form\BookingAdminTicketFormAlter

  contacts_events.form_alter.booking_admin_additional_charges:
    class: Drupal\contacts_events\Form\BookingAdminAdditionalChargesFormAlter

  contacts_events.form_alter.admin_payment_email:
    class: Drupal\contacts_events\Form\AdminPaymentEmailFormAlter

  # Cache backends.
  cache.contacts_events_user_bookings:
    class: Drupal\Core\Cache\CacheBackendInterface
    tags:
      - { name: cache.bin, default_backend: cache.backend.memory }
    factory: cache_factory:get
    arguments: [contacts_events_user_bookings]

  # Response subscribers.
  contacts_events.booking_flow_redirect_subscriber:
    arguments: ['@entity_type.manager']
    class: Drupal\contacts_events\EventSubscriber\BookingFlowRedirectSubscriber
    tags:
      - { name: event_subscriber }

  contacts_events.manual_payment_notifications:
    class: Drupal\contacts_events\ManualPaymentNotifications
    arguments: ['@plugin.manager.mail', '@language.default', '@plugin.manager.element_info', '@?token']
    calls:
      - [setStringTranslation, ['@string_translation']]

  # Email sender.
  contacts_events.admin_payment_email_service:
    class: Drupal\contacts_events\AdminPaymentEmailService
    arguments: ['@config.factory', '@token', '@plugin.manager.mail', '@language_manager', '@commerce_price.currency_formatter']
